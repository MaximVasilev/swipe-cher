﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Gamebase.Systems.GlobalEvents;
using Utilities;

/// <summary>
/// Генератор камней на реке
/// </summary>
public class GeneratorBarrels : MonoBehaviour
{
    [SerializeField] private SettingsBarrels _settings;
    [SerializeField] private SettingsBranches _settingsBranches;
    [SerializeField] private List<GameObject> _barrelsPrefabs;
    [SerializeField] private Transform _fieldWidth; // Настроенная под все варианты разрешений экранов максимальная ширина игрового поля
    
    private GlobalEventsSystem _globalEvents;

    private ObjectsPoolWithArray _pool;
    private bool _gameStatus;

    private void Start()
    {
        _globalEvents = GlobalEventsSystem.Instance;
        _globalEvents.Subscribe(GlobalEventType.LevelDefeat, LevelDefeat);
        _globalEvents.Subscribe(GlobalEventType.LevelRestart, LevelRestart);

        // Назначение зоны генератора бочек пропорционально установленным настройкам к доступному максимуму
        transform.localScale = new Vector3(
            _fieldWidth.transform.localScale.x / 100 * _settingsBranches.FieldWidth,
            transform.localScale.y,
            transform.localScale.z);

        //Аннотация: Опять же объединение объявления и установки значения, избавление от лишней глобальной переменной,
        //плюс использование конструктора сразу с установкой имени вместо доп. строки
        var parent = GameObject.Find("Branches");

        if (parent == null)
            parent = new GameObject("Branches");

        _pool = new ObjectsPoolWithArray(_barrelsPrefabs.ToArray(), 5, parent.transform, true, "Barrels");
        
        StartGenerator();
    }

    private void StartGenerator()
    {
        _gameStatus = true;
        StartCoroutine(GeneratorCoroutine());
    }

    private IEnumerator GeneratorCoroutine()
    {
        while (enabled)
        {
            yield return new WaitForSeconds(_settings.PeriodOfCreation);

            if (_gameStatus)
            {
                //Аннотация: Соединение объявления и установки значения
                var stone = _pool.GetObject(false);

                stone.transform.position = new Vector3(
                    Random.Range(
                        transform.position.x - transform.localScale.x / 2,
                        transform.position.x + transform.localScale.x / 2),
                    transform.position.y,
                    transform.position.z
                );

                stone.SetActive(true);
            }
        }
    }

    private void LevelDefeat()
    {
        _gameStatus = false;
    }

    private void LevelRestart()
    {
        StartCoroutine(LevelRestartCoroutine());
    }

    private IEnumerator LevelRestartCoroutine()
    {
        yield return new WaitForSeconds(3f);
        _gameStatus = true;
    }
}
