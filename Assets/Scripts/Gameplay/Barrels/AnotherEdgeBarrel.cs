﻿using UnityEngine;
using Gamebase.Systems.Debugging;

/// <summary>
/// Получение координат центра бочки
/// </summary>
public class AnotherEdgeBarrel : MonoBehaviour
{
    [SerializeField] private Transform edge;

    public Transform GetEdge()
    {
        return edge;
    }
}
