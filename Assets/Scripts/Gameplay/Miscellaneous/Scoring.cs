﻿using System;
using System.Collections;
using UnityEngine;
using Gamebase.Systems.Resources;
using Gamebase.Systems.GlobalEvents;
using Gamebase.Miscellaneous;

/// <summary>
/// Класс, увеличивающий количество набранных очков с течением времени
/// </summary>
public class Scoring : Singleton<Scoring>
{
    private Action<int> OnChangingScore;

    [SerializeField] private SettingsScore settings;
    [SerializeField] private ChangingScore changing;
    [SerializeField] private MovePlayer movePlayer; // Нужно, чтобы подписаться на событие начала движения игрока (дабы очки не увеличивались, по игрок стоит на начальном камне)
    private ResourcesSystem resources;

    private bool gameStatus;
    
    private void Start()
    {
        resources = ResourcesSystem.Instance;

        OnChangingScore += changing.SetText; // Подписка тектового поля UI на событие изменения количества очков
        GlobalEventsSystem.Instance.Subscribe(GlobalEventType.LevelRestart, LevelRestart);
        ResourcesSystem.Instance.SetResource<ScoreResources>(0);
    }

    public void AddScoring()
    {
        ResourcesSystem.Instance.AddResource<ScoreResources>(1);
        OnChangingScore?.Invoke(resources.GetResourceCount<ScoreResources>());
    }

    private void OnDestroy()
    {
        OnChangingScore = null;
    }

    private void LevelRestart()
    {
        gameStatus = false;
        StartCoroutine(LevelRestartCoroutine());
    }

    private IEnumerator LevelRestartCoroutine()
    {
        yield return new WaitForSeconds(3f);
        gameStatus = true;
    }

    public override void Initialize()
    {
       
    }
}
