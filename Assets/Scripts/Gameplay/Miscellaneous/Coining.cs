﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using Gamebase.Miscellaneous;
using System;

public class Coining : Singleton<Coining>
{
    [SerializeField] private Text _coinText;

    public override void Initialize()
    {
        
    }

    public void AddCoin()
    {
        _coinText.text = (Convert.ToInt32(_coinText.text) + 1).ToString();
    }
}
