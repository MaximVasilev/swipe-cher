﻿using UnityEngine;

/// <summary>
/// Класс, деактивирующий объект в иерархии
/// </summary>
// Должен присутствовать на всех элементах, относящихся к пулам
public class ReturnToPool : MonoBehaviour
{    
    public void Return()
    {
        gameObject.SetActive(false);
    }
}
