﻿using UnityEngine;

/// <summary>
/// Деактивирует объекты в иерархии при выходе их за пределы экрана
/// </summary>
public class Annihilator : MonoBehaviour
{
    private void OnTriggerExit(Collider other)
    {
        other.GetComponent<ReturnToPool>()?.Return();
    }
}
