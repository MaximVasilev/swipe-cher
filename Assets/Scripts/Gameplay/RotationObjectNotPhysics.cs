﻿using Gamebase.Systems.GlobalEvents;
using System.Collections;
using UnityEngine;

internal class RotationObjectNotPhysics : MonoBehaviour
{
    [SerializeField] private SettingsMoveAndRotation _settings;

    private float _rotationSpeed;
    private Vector3 _angularVelocity;
    private bool _onCollision;

    private bool _isGameActive = true;
    private void Start()
    {
        //Аннотация: Нет смысла в кешировании и тем более в создании глобального поля. Б
        GlobalEventsSystem.Instance.Subscribe(GlobalEventType.LevelDefeat, LevelDefeat);
        GlobalEventsSystem.Instance.Subscribe(GlobalEventType.LevelRestart, LevelRestart);
    }

    private void Update()
    {
        if (_isGameActive)
        {
            // Если бревно задело другой объект, то постоянная скорость вращения отключается, чтобы
            // бревно обплыло преграду и назначается новая скорость вращения.
            if (!_onCollision)
            {
                transform.Rotate(_angularVelocity / 10);
            }
        }
    }

    private void OnEnable()
    {
        _rotationSpeed = Random.Range(_settings.RotationSpeed.x, _settings.RotationSpeed.y);
        _angularVelocity = Vector3.up * _rotationSpeed;
        _onCollision = false;
    }

    private void OnCollisionEnter(Collision other)
    {
        _onCollision = true;
    }

    private void OnCollisionExit(Collision other)
    {
        _onCollision = false;
    }

    private void LevelDefeat()
    {
        _isGameActive = false;
    }

    private void LevelRestart()
    {
        if (gameObject.activeInHierarchy)
        {
            StartCoroutine(LevelRestartCoroutine());
        }
        else
        {
            _isGameActive = true;
        }
    }

    private IEnumerator LevelRestartCoroutine()
    {
        yield return new WaitForSeconds(3f);
        _isGameActive = true;
    }
}

