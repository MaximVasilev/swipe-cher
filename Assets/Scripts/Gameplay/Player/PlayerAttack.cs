﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAttack : MonoBehaviour
{
    [SerializeField] private GameObject _prefabArrow;
    [SerializeField] private SettingsPlayer _settings;

    private float _period = 1f;
    private int _indexCurrentDirection = 0;
    [SerializeField] private Animator _animator;

    private List<Vector3> _directions = new List<Vector3>(0);

    private Vector3 _target;

    private void Start()
    {
        StartCoroutine(AttackPeriod());
    }

    private void Update()
    {
        Rotation();
    }

    private void Rotation()
    {
        if (_target != null && _target != Vector3.zero)
            transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.LookRotation(_target), Time.deltaTime * 6);
    }


    private IEnumerator AttackPeriod()
    {
        while (enabled == true)
        {
            yield return new WaitForSeconds(_settings.PeriodAttack);

            if (_directions.Count == 0)
                continue;

            Attack();
        }
    }

    private void Attack()
    {
        if (_indexCurrentDirection >= _directions.Count)
            return;

        var positionArrowStart = new Vector3(transform.position.x, transform.position.y + transform.localScale.y / 2, transform.position.z);
        var arrow = Instantiate(_prefabArrow, positionArrowStart, transform.rotation);
        arrow.GetComponent<Arrow>().Direction = _directions[_indexCurrentDirection];
        
        var moveVector = new Vector3(_directions[_indexCurrentDirection].x, 0, _directions[_indexCurrentDirection].z);
        _target = Vector3.RotateTowards(transform.forward, moveVector, 6, 0);

        StartCoroutine(AttackAnimator());
       
        _indexCurrentDirection++;

        if (_indexCurrentDirection >= _directions.Count)
            _indexCurrentDirection = 0;

       
    }

    private IEnumerator AttackAnimator()
    {
        _animator.SetBool("Attack", true);
        yield return new WaitForSeconds(_animator.GetCurrentAnimatorStateInfo(0).length);
        _animator.SetBool("Attack", false);
        _target = Vector3.RotateTowards(transform.forward, Vector3.forward, 7, 0);
    }    

    public void AddDirections(Vector3 direction)
    {
        _directions.Add(direction);
    }

    public void SetNullDirections()
    {
        _directions.Clear();
        _indexCurrentDirection = 0;
        _target = Vector3.RotateTowards(transform.forward, Vector3.forward, 7, 0);
    }

}
