﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class UnitAnimations : MonoBehaviour
{
    [SerializeField] private SettingsLevel _settingsLevel;
    [SerializeField] private Animator _animator;

    private void Update()
    {
        _animator.speed = (_settingsLevel.SpeedPlayer - 1) * 0.8f;
    }
}
