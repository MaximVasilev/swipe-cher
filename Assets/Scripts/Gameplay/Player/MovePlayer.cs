﻿using System;
using System.Collections;
using UnityEngine;
using Gamebase.Systems.GlobalEvents;

/// <summary>
/// Класс, отвечающий за перемещения игрока
/// </summary>
public class MovePlayer : MonoBehaviour
{
    public Action OnStartMoving; // Событие начало движения после загрузки уровня (чтобы некоторые объекты не двигались, пока игрок стоит на месте)

    [SerializeField] private SettingsPlayer settings;
    [SerializeField] private Transform defaultPosition; // Родительский объект, к которому крепится игрок, когда он не на бревне
    [SerializeField] private GameObject startPosition; // Камень, с которого игрок стартует/возраждается

    private AnimationsPlayer animations;

    private Coroutine runCoroutine;

    private void Start()
    {
        GlobalEventsSystem.Instance.Subscribe(GlobalEventType.LevelRestart, LevelRestart);

        animations = GetComponent<AnimationsPlayer>();
    }


    private void OnDisable()
    {
        GlobalEventsSystem.Instance?.Invoke(GlobalEventType.LevelDefeat);
    }

    // Действия при перезагрузке уровня
    private void LevelRestart()
    {
        transform.parent = defaultPosition;
        startPosition.SetActive(true); // Активация камня, с которого начнется движение
        if (!gameObject.activeInHierarchy)
        {
            gameObject.SetActive(true);
        }
        transform.position = new Vector3(startPosition.transform.position.x, transform.position.y, startPosition.transform.position.z); // И размещение игрока на камне
        animations.Restart(); // Обнуление анимаций
        StartCoroutine(LevelRestartCoroutine());
    }

    private IEnumerator LevelRestartCoroutine()
    {
        yield return new WaitForSeconds(3f); // Ждем три секунды
    }

    private void OnDestroy()
    {
        OnStartMoving = null;
    }
}
