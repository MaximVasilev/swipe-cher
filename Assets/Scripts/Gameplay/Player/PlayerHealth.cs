﻿using Gamebase.Systems.GlobalEvents;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerHealth : MonoBehaviour
{
    [SerializeField] private HpBar _bar;
    [SerializeField] private SettingsPlayer _settings;
    private float _hp;
    
    public float Hp
    {
        get => _hp;
        set
        {
            _hp = value;
            _bar.SetHp(_hp);

            if (_hp < 0)
            {
                Death();
            }
            else if (_hp > _settings.MaxHealth)
            {
                _hp = _settings.MaxHealth;
            }
        }
    }

    private void Start()
    {
        Hp = _settings.MaxHealth;
    }

    private void Update()
    {
        Regeneration();
    }

    private void Regeneration()
    {
        Hp += _settings.SpeedRegen / 10;
    }

    public void Death()
    {
        GlobalEventsSystem.Instance?.Invoke(GlobalEventType.LevelDefeat);
    }
}
