﻿using System;
using System.Collections;
using UnityEngine;
using Gamebase.Miscellaneous;
using Gamebase.Systems.GlobalEvents;
using Utilities;
using Random = UnityEngine.Random;

/// <summary>
/// Генератор buhjrjd
/// </summary>
public class GeneratorEnemies : MonoBehaviour
{
    [SerializeField] private SettingsEnemies _settings;
    [SerializeField] private SettingsLevel _settingsLevel;
    [SerializeField] private Transform fieldWidth; // Настроенная под все варианты разрешений экранов максимальная ширина игрового поля
    [SerializeField] private LayerMask layerBranch;
    private GlobalEventsSystem globalEvents;

    private ObjectsPoolWithArray pool;

    private bool gameStatus = true;
    [SerializeField] private bool _isRandomZ;

    private void Start()
    {
        Init();
    }

    public void Init()
    {
        globalEvents = GlobalEventsSystem.Instance;

        globalEvents.Subscribe(GlobalEventType.LevelDefeat, LevelDefeat);
        globalEvents.Subscribe(GlobalEventType.LevelRestart, LevelRestart);

        // Назначение зоны генератора врагов пропорционально установленным настройкам к доступному максимуму
        transform.localScale = new Vector3(
            fieldWidth.transform.localScale.x / 100 * _settings.FieldWidth,
            transform.localScale.y,
            transform.localScale.z);

        var parent = new GameObject("Enemies");
        pool = new ObjectsPoolWithArray(_settings.Enemies.ToArray(), 20, parent.transform, true, "Enemy");

        //Аннотация: Лучше использовать вместо == и != в операциях сравнения float > и < там где это возможно.
        if (_settings.PeriodOfCreation > 0)
        {
            StartCoroutine(GeneratorCoroutine());
        }
    }

    private IEnumerator GeneratorCoroutine()
    {
        float frequencyOfCreation = _settingsLevel.PeriodSpawnEnemy;
        while (enabled)
        {
            if (gameStatus)
            {
                //Аннотация: Нет смысла объявлять переменную вне этой области. Как правило всегда нужно определять самую узкую область видимости из возможных
                GameObject enemy = pool.GetObject(false);


               
                    // Рандомизация начальной позиции нового врага с учетом его размеров
                    enemy.transform.position = new Vector3(
                            transform.position.x,
                        transform.position.y,
                        Random.Range(
                            transform.position.z - transform.localScale.x / 2,
                            transform.position.z + transform.localScale.x / 2)
                    ) ;
                
                enemy.transform.rotation = Quaternion.Euler(0f, Random.Range(0, 180), 0f);

                // Проверка, есть ли наложения бревен, чтобы бревна не генерировались одно в другом (это нарушает механику движения игрока)
                Collider[] hitColliders = Physics.OverlapBox(
                    enemy.transform.position,
                    enemy.GetComponent<CapsuleCollider>().center / 2,
                    enemy.transform.rotation,
                    layerBranch);
                if (hitColliders.Length != 0)
                {
                    yield return new WaitForSeconds(frequencyOfCreation / 2);
                    continue;
                }

                enemy.SetActive(true);
            }

            yield return new WaitForSeconds(frequencyOfCreation);

            frequencyOfCreation = _settingsLevel.PeriodSpawnEnemy;
        }
    }

    private void LevelDefeat()
    {
        gameStatus = false;
    }

    private void LevelRestart()
    {
        StartCoroutine(LevelRestartCoroutine());
    }

    private IEnumerator LevelRestartCoroutine()
    {
        yield return new WaitForSeconds(3f);
        gameStatus = true;
    }
}
