﻿using Gamebase.Systems.Resources;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Enemy : MonoBehaviour
{
    private NavMeshAgent _agent;
    private float _speedPlayer;
    public Transform Target { get; private set; }

    private void Awake()
    {
        _agent = GetComponent<NavMeshAgent>();
    }

    private void Start()
    {
        Target = GameStatus.Instance.Player.transform;
    }

    private void Update()
    {
        _agent.destination = Target.position;
        _agent.speed = GameStatus.Instance.SettingsLevel.SpeedEnemies;
        _speedPlayer = GameStatus.Instance.SettingsLevel.SpeedPlayer;
        transform.position += Vector3.back * _speedPlayer * Time.deltaTime;
    }

    public void Death()
    {
        if (enabled == false)
            return;

        GetComponent<ReturnToPool>().Return();
        Scoring.Instance.AddScoring();
    }
}
