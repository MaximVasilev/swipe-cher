﻿using Gamebase.Systems.GlobalEvents;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class AttackEnemy : MonoBehaviour
{
    private const string NameAttack = "Attack";
    private const float DistanceAttack = 1.5f;
    
    private Animator _animator;
    private NavMeshAgent _agent;
    private Enemy _move;

    private float _distance;
    private bool _isAttack;

    private void Awake()
    {
        _animator = GetComponent<Animator>();
        _agent = GetComponent<NavMeshAgent>();
        _move = GetComponent<Enemy>();
    }
    private void OnDisable()
    {
        _isAttack = false;
        _animator.SetBool(NameAttack, false);
    }

    private void Update()
    {
        if (_agent.destination == null)
            return;

        if (_isAttack == false)
        {
            _distance = Vector3.Distance(transform.position, _agent.destination);

            if (_distance <= DistanceAttack)
            {
                StartCoroutine(OnAttack());
            }
        }
    }


    private IEnumerator OnAttack()
    {
        _isAttack = true;

       // _move?.SetSpeedZero();
        _animator.SetBool(NameAttack, true);

        var health = _move.Target.GetComponent<PlayerHealth>();
        if (health != null)
            health.Hp -= 20;

        yield return new WaitForSeconds(_animator.GetCurrentAnimatorStateInfo(0).length);

        _animator.SetBool(NameAttack, false);
      //  _move?.SetSpeedDefault();

        _isAttack = false;
    }
}
