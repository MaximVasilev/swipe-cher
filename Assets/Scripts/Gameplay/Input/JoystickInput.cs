﻿using System.Collections;
using System.Security.Policy;
using UnityEngine;
using UnityEngine.EventSystems;


public class JoystickInput : MonoBehaviour
{

    private Vector3 _mousePositionDown;
    private Vector3 _inputCurrentVector;

    public Vector3 InputVector => _inputCurrentVector;
    public float Angle;
    private void Start()
    {
        TouchInput.Instance.OnTouch += Touch;
    }

    private void OnDisable()
    {
        TouchInput.Instance.OnTouch -= Touch;
    }

    private Vector3 _inputCurrentVectorClamp = Vector3.zero;
    private float _maxLength;

    private bool _isMoreMax = false;
    public bool _isDecrease;

    private void Update()
    {
        // Если зажали
        if (Input.GetMouseButton(0) == true)
        {
            _inputCurrentVector =  Input.mousePosition - _mousePositionDown;
        }
    }

    private void Touch(bool isToush)
    {
        if(isToush == true)
        {
            _mousePositionDown = Input.mousePosition;
        }
        else
        {
            _inputCurrentVector = Vector3.zero;
            _mousePositionDown = Vector3.zero;
        }
    }
}
