﻿using System;
using System.Collections;
using UnityEngine;
using Gamebase.Miscellaneous;
using Gamebase.Systems.GlobalEvents;
using UnityEngine.EventSystems;

/// <summary>
/// Класс, следящий за нажатием на экран и вызывающий соответствующее событие для наблюдателей
/// </summary>
public class TouchInput : Singleton<TouchInput>, IPointerDownHandler, IPointerUpHandler
{
    [SerializeField] private SettingsTouch _settings;
    public Action<bool> OnTouch;
    public Action OnClick;

    private bool gameStatus = true;

    private bool _isDown;
    private float _timeClick;

    public override void Initialize()
    { 
        GlobalEventsSystem.Instance.Subscribe(GlobalEventType.LevelDefeat, LevelDefeat);
        GlobalEventsSystem.Instance.Subscribe(GlobalEventType.LevelRestart, LevelRestart);
    }

    private void Update()
    {
        if (_isDown == true)
            _timeClick += Time.deltaTime;
    }
    private void OnDestroy()
    {
        OnTouch = null;
    }

    private void LevelDefeat()
    {
        gameStatus = false;
    }

    private void LevelRestart()
    {
        StartCoroutine(LevelRestartCoroutine());
    }

    private IEnumerator LevelRestartCoroutine()
    {
        yield return new WaitForSeconds(3f);
        gameStatus = true;
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        if (gameStatus == true)
        {
            OnTouch?.Invoke(true);
            _isDown = true;
        }
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        if (gameStatus == true)
        {
            OnTouch?.Invoke(false);
            
            if (_timeClick < _settings.TimeClick)
            {
                OnClick?.Invoke();
            }

            _isDown = false;
            _timeClick = 0;
        }
    }
}
