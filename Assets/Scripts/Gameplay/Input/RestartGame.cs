﻿using UnityEngine;
using Gamebase.Systems.GlobalEvents;

/// <summary>
/// Класс вызова события перезагрузки уровня по нажатию на кнопку рестарта.
/// </summary>
public class RestartGame : MonoBehaviour
{
    private GlobalEventsSystem globalEvents;

    private void Start()
    {
        globalEvents = GlobalEventsSystem.Instance;
    }

    public void OnClickRestartButton()
    {
        globalEvents?.Invoke(GlobalEventType.LevelRestart);
    }
}
