﻿using UnityEngine;
using UnityEngine.SceneManagement;
using Gamebase.Systems.GlobalEvents;

/// <summary>
/// Переход на сцену с меню по нажатию кнопки в диалоговом окне проигрыша или по нажатию кнопки "Назад" на смартфоне
/// </summary>
public class ExitToMenu : MonoBehaviour
{

    public void OnExitButton()
    {
        GlobalEventsSystem.Instance.ClearAllEvents(); // Нужно, т.к. GlobalEventsSystem не всегда вовремя отписывает всех от событий, из-за чего могут возникать ошибки
        SceneManager.LoadScene(0, LoadSceneMode.Single);
    }

    private void Update()
    {
        if (Input.GetKey(KeyCode.Escape))
        {
            OnExitButton();
        }
    }
}
