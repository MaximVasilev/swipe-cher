﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveRoad : MonoBehaviour
{
    [SerializeField] private SettingsLevel settings;

    private void Update()
    {
        transform.position += Vector3.back * settings.SpeedPlayer * Time.deltaTime;
        if (transform.position.z < -15)
            transform.position = new Vector3(transform.position.x, transform.position.y, 15);
    }
}
