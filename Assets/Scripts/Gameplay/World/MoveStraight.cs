﻿using System.Collections;
using UnityEngine;
using Gamebase.Systems.GlobalEvents;

/// <summary>
/// Движение прямо без поворотов
/// </summary>
public class MoveStraight : MonoBehaviour
{
    [SerializeField] private SettingsLevel settings;
    private bool gameStatus = true;
    
    private void Start()
    {
        GlobalEventsSystem.Instance.Subscribe(GlobalEventType.LevelDefeat, LevelDefeat);
        GlobalEventsSystem.Instance.Subscribe(GlobalEventType.LevelRestart, LevelRestart);
    }

    private void Update()
    {
        if (gameStatus)
        {
            transform.position += Vector3.back * settings.SpeedPlayer * Time.deltaTime;
        }
    }

    private void LevelDefeat()
    {
        gameStatus = false;
        GetComponent<ReturnToPool>().Return();
    }

    private void LevelRestart()
    {
        if (gameObject.activeInHierarchy)
        {
            StartCoroutine(LevelRestartCoroutine());
        }
    }

    private IEnumerator LevelRestartCoroutine()
    {
        yield return new WaitForSeconds(3f);
        gameStatus = true;
    }
}
