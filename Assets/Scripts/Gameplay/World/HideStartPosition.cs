﻿using UnityEngine;

/// <summary>
/// Исчезновение стартового камня, когда игрок сделал прыжок с него
/// </summary>
public class HideStartPosition : MonoBehaviour
{
    [SerializeField] private LayerMask branchLayer;

    private void OnEnable()
    {
        // А также уничтожение бревен со стартовой позиции, чтобы не мешались, когда был рестарт и нужно поставить игрока
        Collider[] hitColliders = Physics.OverlapSphere(transform.position, GetComponent<SphereCollider>().radius, branchLayer);
        if (hitColliders.Length != 0)
        {
            foreach (var item in hitColliders)
            {
                item.GetComponent<ReturnToPool>().Return();
            }
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            gameObject.SetActive(false);
        }
    }
}
