﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Gamebase.Systems.GlobalEvents;
using Utilities;

/// <summary>
/// Генератор камней на реке
/// </summary>
public class GeneratorStones : MonoBehaviour
{
    [SerializeField] private SettingsStones _settings;
    [SerializeField] private List<GameObject> stonePrefabs;
    [SerializeField] private SettingsBranches settingsBranches;
    [SerializeField] private Transform fieldWidth; // Настроенная под все варианты разрешений экранов максимальная ширина игрового поля
    [SerializeField] private MovePlayer movePlayer; // Нужно, чтобы подписаться на событие начала движения игрока ("дно реки" же не движется, когда игрок стоит на месте)
    private GlobalEventsSystem globalEvents;

    private ObjectsPoolWithArray pool;
    private bool gameStatus;
    private float _period;
    private void Start()
    {
        globalEvents = GlobalEventsSystem.Instance;
        _period = _settings.PeriodOfCreation;
        globalEvents.Subscribe(GlobalEventType.LevelDefeat, LevelDefeat);
        globalEvents.Subscribe(GlobalEventType.LevelRestart, LevelRestart);

        movePlayer.OnStartMoving += StartGenerator; // Подписка на событие начала движения игрока

        // Назначение зоны генератора камней пропорционально установленным настройкам к доступному максимуму
        transform.localScale = new Vector3(
            fieldWidth.transform.localScale.x / 100 * settingsBranches.FieldWidth,
            transform.localScale.y,
            transform.localScale.z);

        //Аннотация: Опять же объединение объявления и установки значения, избавление от лишней глобальной переменной,
        //плюс использование конструктора сразу с установкой имени вместо доп. строки
        var parent = new GameObject("Stones").transform;

        pool = new ObjectsPoolWithArray(stonePrefabs.ToArray(), 5, parent, true, "Stones");
    }

    private void StartGenerator()
    {
        gameStatus = true;
        StartCoroutine(GeneratorCoroutine(_period));
    }

    private IEnumerator GeneratorCoroutine(float frequencyOfCreation)
    {
        while (enabled)
        {
            yield return new WaitForSeconds(frequencyOfCreation);

            if (gameStatus)
            {
                //Аннотация: Соединение объявления и установки значения
                var stone = pool.GetObject(false);

                stone.transform.position = new Vector3(
                    Random.Range(
                        transform.position.x - transform.localScale.x / 2,
                        transform.position.x + transform.localScale.x / 2),
                    transform.position.y,
                    transform.position.z
                );

                stone.SetActive(true);
            }
        }
    }

    private void LevelDefeat()
    {
        gameStatus = false;
    }

    private void LevelRestart()
    {
        StartCoroutine(LevelRestartCoroutine());
    }

    private IEnumerator LevelRestartCoroutine()
    {
        yield return new WaitForSeconds(3f);
        gameStatus = true;
    }
}
