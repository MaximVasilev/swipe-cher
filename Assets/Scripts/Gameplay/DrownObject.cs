﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DrownObject : MonoBehaviour
{
    private const float DrownTimeDelay = 3f;

    public event Action Drown;

    private bool _isDrown;
    private float _positionYDrown;

    /// <summary>
    /// Тонуть.
    /// </summary>
    public void OnDrown()
    {
        _isDrown = true;
        StartCoroutine(DrownDelay());
    }

    private void Start()
    {
        _isDrown = false;
        _positionYDrown = transform.position.y - transform.localScale.y / 2;
    }

    private void FixedUpdate()
    {
        if (_isDrown == false)
            return;

        var positionDrown = new Vector3(transform.position.x, _positionYDrown, transform.position.z);
        transform.position = Vector3.MoveTowards(transform.position, positionDrown, Time.deltaTime / (DrownTimeDelay* DrownTimeDelay));
    }



    private IEnumerator DrownDelay()
    {
        yield return new WaitForSeconds(DrownTimeDelay);
        _isDrown = false;

        Drown?.Invoke();
    }    
}
