﻿using Gamebase.Systems.GlobalEvents;
using System.Collections;
using UnityEngine;

internal class MoveObject : MonoBehaviour
{
    [SerializeField] private SettingsMoveAndRotation _settings;

    private float _moveSpeed;
    private Vector3 _velocity;
    private bool _isGameActive = true;
    private bool _onCollision;

    private Rigidbody _rigidbody;

    private void Start()
    {
        //Аннотация: Нет смысла в кешировании и тем более в создании глобального поля. Б
        GlobalEventsSystem.Instance.Subscribe(GlobalEventType.LevelDefeat, LevelDefeat);
        GlobalEventsSystem.Instance.Subscribe(GlobalEventType.LevelRestart, LevelRestart);

        _rigidbody = GetComponent<Rigidbody>();
    }

    private void Update()
    {
        if (_isGameActive)
        {
            // Бревно движется с постоянной скоростью
            _rigidbody.velocity = _velocity;
        }
    }

    private void OnEnable()
    {
        // Назначение рандомных скоростей из диапазона из настроек
        _moveSpeed = Random.Range(_settings.MoveSpeed.x, _settings.MoveSpeed.y);
        _velocity = Vector3.back * _moveSpeed;

        _onCollision = false;
    }

    private void OnCollisionEnter(Collision other)
    {
        _onCollision = true;
    }

    private void OnCollisionExit(Collision other)
    {
        _onCollision = false;
    }

    private void LevelDefeat()
    {
        _isGameActive = false;
        _rigidbody.velocity = Vector3.zero;
    }

    private void LevelRestart()
    {
        if (gameObject.activeInHierarchy)
        {
            StartCoroutine(LevelRestartCoroutine());
        }
        else
        {
            _isGameActive = true;
        }
    }

    private IEnumerator LevelRestartCoroutine()
    {
        yield return new WaitForSeconds(3f);
        _isGameActive = true;
    }
}

