﻿using System;
using System.Collections;
using UnityEngine;
using Gamebase.Miscellaneous;
using Gamebase.Systems.GlobalEvents;
using Utilities;
using Random = UnityEngine.Random;

/// <summary>
/// Генератор бревен
/// </summary>
public class GeneratorBranches : Singleton<GeneratorBranches>
{
    /// <summary>
    /// Событие создания нового бревна
    /// </summary>
    public Action<GameObject> OnCreatedBranch;

    [SerializeField] private SettingsBranches settings;
    [SerializeField] private Transform fieldWidth; // Настроенная под все варианты разрешений экранов максимальная ширина игрового поля
    [SerializeField] private LayerMask layerBranch;
    private GlobalEventsSystem globalEvents;

    private ObjectsPoolWithArray pool;

    private bool gameStatus = true;

    public override void Initialize()
    {
        globalEvents = GlobalEventsSystem.Instance;

        globalEvents.Subscribe(GlobalEventType.LevelDefeat, LevelDefeat);
        globalEvents.Subscribe(GlobalEventType.LevelRestart, LevelRestart);

        // Назначение зоны генератора бревен пропорционально установленным настройкам к доступному максимуму
        transform.localScale = new Vector3(
            fieldWidth.transform.localScale.x / 100 * settings.FieldWidth,
            transform.localScale.y,
            transform.localScale.z);

        var parent = GameObject.Find("Branches");
        
        if (parent == null)
            parent = new GameObject("Branches");

        pool = new ObjectsPoolWithArray(settings.Branches.ToArray(), 20, parent.transform, true, "Branch");
        
        //Аннотация: Лучше использовать вместо == и != в операциях сравнения float > и < там где это возможно.
        if (settings.PeriodOfCreation > 0)
        {
            StartCoroutine(GeneratorCoroutine());
        }
    }

    private IEnumerator GeneratorCoroutine()
    {
        float frequencyOfCreation = settings.PeriodOfCreation;
        while (enabled)
        {
            if (gameStatus)
            {
                //Аннотация: Нет смысла объявлять переменную вне этой области. Как правило всегда нужно определять самую узкую область видимости из возможных
                GameObject branch = pool.GetObject(false);

                // Рандомизация начальной позиции нового бревна с учетом его размеров (чтобы бревно не возникало в береге)
                branch.transform.position = new Vector3(
                    Random.Range(
                        transform.position.x - transform.localScale.x / 2 + branch.GetComponent<BoxCollider>().size.x / 2,
                        transform.position.x + transform.localScale.x / 2 - branch.GetComponent<BoxCollider>().size.x / 2),
                    transform.position.y,
                    transform.position.z
                );
                branch.transform.rotation = Quaternion.Euler(0f, Random.Range(0, 180), 0f);

                // Проверка, есть ли наложения бревен, чтобы бревна не генерировались одно в другом (это нарушает механику движения игрока)
                Collider[] hitColliders = Physics.OverlapBox(
                    branch.transform.position,
                    branch.GetComponent<BoxCollider>().size / 2,
                    branch.transform.rotation,
                    layerBranch);
                if (hitColliders.Length != 0)
                {
                    yield return new WaitForSeconds(frequencyOfCreation / 2);
                    continue;
                }

                branch.SetActive(true);
                OnCreatedBranch?.Invoke(branch);
            }

            yield return new WaitForSeconds(frequencyOfCreation);

            frequencyOfCreation = settings.PeriodOfCreation;
        }
    }

    private void OnDestroy()
    {
        OnCreatedBranch = null;
    }

    private void LevelDefeat()
    {
        gameStatus = false;
    }

    private void LevelRestart()
    {
        StartCoroutine(LevelRestartCoroutine());
    }

    private IEnumerator LevelRestartCoroutine()
    {
        yield return new WaitForSeconds(3f);
        gameStatus = true;
    }
}
