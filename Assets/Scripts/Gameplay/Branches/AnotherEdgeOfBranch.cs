﻿using UnityEngine;
using Gamebase.Systems.Debugging;

/// <summary>
/// Получение координат краев бревен
/// </summary>
public class AnotherEdgeOfBranch : MonoBehaviour
{
    [SerializeField] private Transform edge1;
    [SerializeField] private Transform edge2;


    public Transform GetOppositeEdge(Transform edge)
    {
        if (edge == edge1)
        {
            return edge2;
        }
        else if (edge == edge2)
        {
            return edge1;
        }
        else
        {
            DebugSystem.LogError("Неверно указан аргумент GetOppositeEdge!");
            return null;
        }
    }

    /// <summary>
    /// Получить ближний к низу экрана край бревна
    /// </summary>
    /// <returns></returns>
    public Transform GetFromEdge()
    {
        return edge1.position.z < edge2.position.z ? edge1 : edge2;
    }

    /// <summary>
    /// Получить противоположный край бревна
    /// </summary>
    /// <param name="current">Позиция, от которой проверяем</param>
    /// <returns></returns>
    public Transform GetToEdge(Transform current)
    {
        //Аннотация: Также как в GetFromEdge тут можно использовать более короткую запись.
        // Сравнение дистанции до краев бревна. Какая больше, такую координату и возвращаем
        return Vector3.Distance(edge1.transform.position, current.transform.position) > Vector3.Distance(edge2.transform.position, current.transform.position) 
            ? edge1 : edge2;
    }

    public Transform GetNeighbourEdge(Transform current)
    {
        if (edge1 == current)
            return edge2;
        else
            return edge1;
    }
}
