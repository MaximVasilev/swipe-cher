﻿using UnityEngine;
using Gamebase.Systems.GlobalEvents;

/// <summary>
/// Изменение слоя бревна для игнорирования лучей, когда игрок находится на бревне
/// </summary>
public class DisableLayerBranch : MonoBehaviour
{
    [SerializeField] private GameObject parent;
    private const int LAYER_DEFAULT = 0; // Стандартный слой
    private const int LAYER_BRANCH = 8; // Слой бревен

    private void Start()
    {
        //Аннотация: Нет смысла в кешировании и тем более в создании глобального поля.
        //Это нужно если ссылка используется очень часто и в разных местах класса, чтобы не получать ее каждый раз через свойство.
        //А тут все в одном месте и всего один раз.
        GlobalEventsSystem.Instance.Subscribe(GlobalEventType.LevelRestart, LevelRestart);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            parent.layer = LAYER_DEFAULT;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            parent.layer = LAYER_BRANCH;
        }
    }

    /// <summary>
    /// Назначение слоя "бревна", нужно, чтобы при рестарте не было "невидимых" для игрока бревен
    /// </summary>
    private void LevelRestart()
    {
        parent.layer = LAYER_BRANCH;
    }
}
