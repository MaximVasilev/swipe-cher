﻿using UnityEngine;
using Gamebase.Systems.Resources;

/// <summary>
/// Класс сбора монет игроком и передачи значения "хранителю"
/// (а также удаления монет в пул, если случайно одновременно несколько монет сгенерировались на одном бревне)
/// </summary>
public class CollectCoin : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player") && other.transform.parent == transform.parent)
        {
            ResourcesSystem.Instance.AddResource<CoinsResources>(1);
            Coining.Instance.AddCoin();
            GetComponent<ReturnToPool>().Return();
        }

        if (other.CompareTag("Coin"))
        {
            GetComponent<ReturnToPool>().Return();
        }
    }
}
