﻿using UnityEngine;
using Gamebase.Miscellaneous;

/// <summary>
/// Генератор монет
/// </summary>
public class GeneratorCoins : MonoBehaviour
{
    [SerializeField] private SettingsCoins settings;
    private ObjectsPool pool;
    
    private void Start()
    {
        GeneratorBranches.Instance.OnCreatedBranch += CreateCoin;

        pool = new ObjectsPool(settings.CoinPrefab, 10, true, gameObject.transform);
    }

    private void CreateCoin(GameObject branch)
    {
        // При генерации бревна получается рандомное число. Если оно меньше указанного в
        // настройках процента, то помещаем монету в центр бревна.
        if ((int)Random.Range(0f, 100f) < settings.ProbabilityCreatingCoin)
        {
            //Не нужно разделять объявление и назначение там где это возможно. +Всегда проще использовать var
            var coin = pool.GetObject(false);
            coin.transform.position = branch.transform.position;
            coin.transform.parent = branch.transform;
            coin.SetActive(true);
        }
    }
}
