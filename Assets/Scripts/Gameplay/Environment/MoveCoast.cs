﻿using System.Collections;
using UnityEngine;

/// <summary>
/// Перемещение берегов к центру экрана
/// </summary>
public class MoveCoast : MonoBehaviour
{
    private const float SPEED = 0.1f; // Оптимальная скорость сближения берегов
    [SerializeField] private Transform target; // Центр игрового пространства
    private Coroutine moveCoroutine;

    private void Start()
    {
        moveCoroutine = StartCoroutine(MoveCoroutine());
    }

    private IEnumerator MoveCoroutine()
    {
        while (enabled)
        {
            transform.position = Vector3.Lerp(transform.position, target.position, SPEED * Time.deltaTime);
            yield return new WaitForFixedUpdate();
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        // Прекращение движения, когда берег достиг ограничителя, имеющего размеры, устанавливаемые в ChangeLimitForCoast
        if (other.CompareTag("Limiter"))
        {
            StopCoroutine(moveCoroutine);
        }
    }
}
