﻿using UnityEngine;

/// <summary>
/// Класс, меняющий размер коллайдера, при соприкосновении с которым берега перестают сдвигаться.
/// </summary>
public class ChangeLimitForCoast : MonoBehaviour
{
    [SerializeField] private Transform fieldWidth;
    
    private void Start()
    {
        // Назначение размера пропорционально установленным настройкам к доступному максимуму
        transform.localScale = new Vector3(
            fieldWidth.transform.localScale.x,
            transform.localScale.y,
            transform.localScale.z);
    }
}
