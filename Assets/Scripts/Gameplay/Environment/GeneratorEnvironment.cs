﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;
using Gamebase.Systems.GlobalEvents;
using Utilities;

/// <summary>
/// Генератор игрового окружения (деревьев, грибов и т.д.) на берегах
/// </summary>
public class GeneratorEnvironment : MonoBehaviour
{
    [AssetList(Path = "/Prefabs/World/", AutoPopulate = true)]
    [SerializeField] private List<GameObject> prefabs;
    [SerializeField] private MovePlayer movePlayer; // Нужно, чтобы подписаться на событие начала движения игрока (берег же не движется, когда игрок стоит на месте)
    [SerializeField] private Transform parent;
    [SerializeField] private SettingsLevel _settings;

    private ObjectsPoolWithArray pool;
    private bool gameStatus;

    private void Start()
    {
        GlobalEventsSystem.Instance.Subscribe(GlobalEventType.LevelDefeat, LevelDefeat);
        GlobalEventsSystem.Instance.Subscribe(GlobalEventType.LevelRestart, LevelRestart);

        movePlayer.OnStartMoving += StartGenerator; // Подписаться на событие начала движения игрока

        pool = new ObjectsPoolWithArray(prefabs.ToArray(), 10, parent, true);
        StartGenerator();
    }

    private void StartGenerator()
    {
        gameStatus = true;
        StartCoroutine(GeneratorCoroutine(_settings.PeriodSpawnItems));
    }

    private IEnumerator GeneratorCoroutine(float frequencyOfCreation)
    {
        GameObject prefab;

        while (enabled)
        {
             // Немного рандомности между временем создания новых объектов, чтобы они не были выстроены по линейке
            yield return new WaitForSeconds(Random.Range(_settings.PeriodSpawnItems - _settings.PeriodSpawnItems / 3, _settings.PeriodSpawnItems + _settings.PeriodSpawnItems / 3));

            if (gameStatus)
            {
                prefab = GetPrefab();
            }
        }
    }

    private GameObject GetPrefab()
    {
        GameObject prefab = pool.GetObject(false);

        prefab.transform.position = new Vector3(
                    Random.Range(
                        transform.position.x - transform.localScale.x / 2 + prefab.transform.localScale.x / 2,
                        transform.position.x + transform.localScale.x / 2 - prefab.transform.localScale.x / 2),
                    transform.position.y,
                    transform.position.z
                );
        prefab.SetActive(true);
        return prefab;
    }

    private void LevelDefeat()
    {
        gameStatus = false;
    }

    private void LevelRestart()
    {
        StartCoroutine(LevelRestartCoroutine());
    }

    private IEnumerator LevelRestartCoroutine()
    {
        yield return new WaitForSeconds(3f);
        gameStatus = true;
    }
}
