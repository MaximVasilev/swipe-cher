﻿using System.Collections;
using UnityEngine;
using Gamebase.Systems.GlobalEvents;

/// <summary>
/// Перемещение объектов игрового окружения (деревья, грибы и т.д.) по берегу
/// </summary>
public class MoveEnvironment : MonoBehaviour
{
    private float _speed;
    private bool gameStatus = true;

    private void Start()
    {
        GlobalEventsSystem.Instance.Subscribe(GlobalEventType.LevelDefeat, LevelDefeat);
        GlobalEventsSystem.Instance.Subscribe(GlobalEventType.LevelRestart, LevelRestart);
    }

    private void Update()
    {
        if (gameStatus)
        {
            _speed = GameStatus.Instance.SettingsLevel.SpeedPlayer;
            transform.position += Vector3.back * _speed * Time.deltaTime;
        }
    }

    private void LevelDefeat()
    {
        gameStatus = false;
        GetComponent<ReturnToPool>().Return();
    }

    private void LevelRestart()
    {
        if (gameObject.activeInHierarchy)
        {
            StartCoroutine(LevelRestartCoroutine());
        }
    }

    private IEnumerator LevelRestartCoroutine()
    {
        yield return new WaitForSeconds(3f);
        gameStatus = true;
    }
}
