﻿using System.Collections;
using UnityEngine;
using Gamebase.Systems.Resources;

/// <summary>
/// Передача значения рекорда на экране меню в UI
/// </summary>
public class RecordTransmitterForMenu : MonoBehaviour
{
    private void Start()
    {
        StartCoroutine(StartDelay()); // Небольшая задержка, чтобы успел загрузиться менеджер ресурсов и не возник NullExseption
    }

    private IEnumerator StartDelay()
    {
        yield return new WaitForFixedUpdate();
        GetComponent<MenuController>().SetBestScoreText(ResourcesSystem.Instance.GetResourceCount<BestScoreResources>());
    }
}
