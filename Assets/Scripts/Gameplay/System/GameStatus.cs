﻿using UnityEngine;
using Gamebase.Systems.GlobalEvents;
using Gamebase.Miscellaneous;

/// <summary>
/// Класс, включающий/выключающий "паузу" в игре
/// </summary>
public class GameStatus : Singleton<GameStatus>
{
    [SerializeField] private SettingsLevel _settingsLevel;
    [SerializeField] private GameObject _player;


    public SettingsLevel SettingsLevel => _settingsLevel;
    public GameObject Player => _player;

    private void Start()
    {
        Time.timeScale = 1f;

        GlobalEventsSystem.Instance.Subscribe(GlobalEventType.LevelDefeat, LevelDefeat);
        GlobalEventsSystem.Instance.Subscribe(GlobalEventType.LevelRestart, LevelRestart);
    }

    private void OnDestroy()
    {
        Time.timeScale = 1f;
    }

    private void LevelDefeat()
    {
        Time.timeScale = 0f;
    }

    private void LevelRestart()
    {
        Time.timeScale = 1f;
    }

    public override void Initialize()
    {
        
    }
}
