﻿using UnityEngine;
using Gamebase.Systems.GlobalEvents;
using Gamebase.Systems.Resources;
using System;

/// <summary>
/// Класс, следящий за количеством набранных очков и монет, передающим значения в UI и отнимающий монеты при рестарте
/// </summary>
public class GamePlayerPrefs : MonoBehaviour
{
    private Action<int, int, int, int> OnDefeat;

    [SerializeField] private SettingsCoins settingsCoins;
    [SerializeField] private UpdateValues updateValues;
    private ResourcesSystem resources;
    
    private void Start()
    {
        resources = ResourcesSystem.Instance;

        GlobalEventsSystem.Instance.Subscribe(GlobalEventType.LevelDefeat, LevelDefeat);
        GlobalEventsSystem.Instance.Subscribe(GlobalEventType.LevelRestart, LevelRestart);
        OnDefeat += updateValues.GetValues; // Подписка элементов UI на событие, когда был проигрыш и нужно обновить значения
    }

    private void OnDestroy()
    {
        OnDefeat = null;
    }

    private void LevelDefeat()
    {

        resources.SetResource<BestScoreResources>(resources.GetResourceCount<ScoreResources>());
        /*
            <param name="currentValue">Количество набранных очков за сессию</param>
            <param name="recordValue">Рекорд игры</param>
            <param name="coinsValue">Количество собранных монет</param>
            <param name="costValue">Стоимость воскрешения</param>
        */
        OnDefeat?.Invoke(resources.GetResourceCount<ScoreResources>(), resources.GetResourceCount<BestScoreResources>(), 0, 0);
    }
        
    private void LevelRestart()
    {
        // Отнимание монет за рестарт
        resources.SetResource<CoinsResources>(resources.GetResourceCount<CoinsResources>() - settingsCoins.CostRestart);
    }
}
