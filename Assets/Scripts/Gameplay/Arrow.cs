﻿using UnityEngine;
using System.Collections;

public class Arrow : MonoBehaviour
{
    private float _speedArrow;
    private float _speedPlayer;

    private float _timeHealth;

    public Vector3 Direction;
    public Rigidbody _rigidbody;
    private void Start()
    {
        _rigidbody = GetComponent<Rigidbody>();
        StartCoroutine(UpdateDelay());
    }

    private IEnumerator UpdateDelay()
    {
       // yield return new WaitForSeconds(0.3f);
        transform.LookAt(Direction);

        _speedArrow = GameStatus.Instance.SettingsLevel.ForceArrow;
        _rigidbody.AddForce(transform.forward * _speedArrow * 100);
        while (enabled == true)
        {
            
            _speedPlayer = GameStatus.Instance.SettingsLevel.SpeedPlayer;

            transform.position += Vector3.back * _speedPlayer * Time.deltaTime;
            _timeHealth += Time.deltaTime;

            if (_timeHealth > 3)
                Destroy(gameObject);

            yield return new WaitForFixedUpdate();
        }
    }

    private void OnTriggerEnter(Collider collider)
    {
        var enemy = collider.GetComponent<Enemy>();
        if (enemy != null)
            enemy.Death();

        var rock = collider.gameObject.tag == "Branch";
        if (rock == true)
            Destroy(gameObject);
    }
}
