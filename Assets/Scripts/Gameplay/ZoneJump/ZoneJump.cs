﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Зона прыжка.
/// </summary>
public class ZoneJump : MonoBehaviour
{
    private void OnEnable()
    {
        TouchInput.Instance.OnTouch += Touch;
    }
        
    private void OnDisable()
    {
        TouchInput.Instance.OnTouch -= Touch;
    }

    public void Touch(bool isTouch)
    {
        if(isTouch == false)
           StartCoroutine(DestroyDelay());
    }

    private IEnumerator DestroyDelay()
    {
        yield return new WaitForFixedUpdate();
            Destroy(gameObject);
    }
}
