﻿using UnityEngine;
using System.Collections;
using Gamebase.Systems.Debugging;
using Sirenix.Utilities;
using UnityEngine.EventSystems;
using System.Linq;

public class InputArrows : MonoBehaviour
{
    [SerializeField] private JoystickInput _joystick;
    [SerializeField] private PlayerAttack _attack;
    [SerializeField] private Transform _parentArrows;
    [SerializeField] private GameObject _arrowPrefab;


    private void Start()
    {
        if (_joystick == null)
        {
            DebugSystem.LogError("Добавьте JoystickInput в InputArrows объекта " + name);
            return;
        }
        TouchInput.Instance.OnTouch += Touch;
        TouchInput.Instance.OnClick += Click;
    }
    private void OnDisable()
    {
       // TouchInput.Instance.OnTouch -= Touch;
    }


    private Vector3 _positionZone;
    private GameObject _newArrow;
    private void Update()
    {
        if (Input.GetMouseButton(0) == true)
        {
            if (_newArrow == null)
                return;

            _positionZone = transform.position;
            _positionZone = new Vector3(_positionZone.x + _joystick.InputVector.x, _newArrow.transform.position.y, _positionZone.z + _joystick.InputVector.y);

            _parentArrows.transform.LookAt(_positionZone, transform.up);
        }

    }

    private void Touch(bool isTouch)
    {
        if (isTouch)
        {
            _newArrow = Instantiate(_arrowPrefab, _parentArrows);
        }
        else
        {
            _newArrow.transform.parent = _parentArrows.parent;
            _attack.AddDirections(_positionZone);
        }
    }

    private void Click()
    {
        _attack.SetNullDirections();

        var arrows = _parentArrows.parent.GetComponentsInChildren<Transform>().ToList();
        arrows.Remove(_parentArrows.parent);
        arrows.Remove(_parentArrows);

        foreach (var arrow in arrows)
        {
            Destroy(arrow.gameObject);
        }
    }
}
