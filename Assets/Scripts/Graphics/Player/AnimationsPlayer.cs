﻿using UnityEngine;

/// <summary>
/// Класс, предоставляющий быстрый доступ к режимам аниматора игрока
/// </summary>
public class AnimationsPlayer : MonoBehaviour
{
    [SerializeField] private Animator animator;

    //Аннотация: такой подход позволяет избавиться от поиска по строке при каждом использовании триггера.
    //Имеет смысл если анимация вызывается много раз в течение игры
    private int speedTrigger = Animator.StringToHash("Speed");
    private int jumpTrigger = Animator.StringToHash("Jump");
    private int diveTrigger = Animator.StringToHash("Dive");

    /// <summary>
    /// Перезапуск аниматора
    /// </summary>
    public void Restart()
    {
        animator.Rebind();
        animator.SetFloat(speedTrigger, 0f);
        animator.SetBool(jumpTrigger, false);
        animator.SetBool(diveTrigger, false);
    }

    /// <summary>
    /// Изменение скорости
    /// </summary>
    /// <param name="speed">Да - игрок побежит, нет - остановится на месте</param>
    public void Speed(bool speed)
    {
        animator.SetFloat(speedTrigger, speed ? 1f : 0f);
    }

    /// <summary>
    /// Ныряние в воду
    /// </summary>
    /// <param name="dive">Да - игрок ныряет, нет - отключение параметра</param>
    public void Dive(bool dive)
    {
        animator.SetBool(diveTrigger, dive);
    }

    /// <summary>
    /// Совершение прыжка
    /// </summary>
    /// <param name="jump">Да - игрок прыгает, нет - отключение параметра</param>
    public void Jump(bool jump)
    {
        animator.SetBool(jumpTrigger, jump);
    }
}
