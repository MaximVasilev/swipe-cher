﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

/// <summary>
/// Класс, отвечающий за элементы UI в главном меню и срабатывание на нажатие кнопки "Назад" в смартфоне
/// </summary>
public class MenuController : MonoBehaviour
{
    [SerializeField] private Text bestScoreText;

    
    private void Update()
    {
        //Аннотация: Для контроля кнопки назад (она же escape на PC) в Doozy есть готовый функционал, с использованием Nody. Очень удобно
        if (Input.GetKey(KeyCode.Escape))
        {
            Application.Quit();
        }
    }

    public void OnStartButton()
    {
        SceneManager.LoadScene(1, LoadSceneMode.Single); // Загрузка игровой сцены
    }

    public void SetBestScoreText(int value)
    {
        bestScoreText.text = $"Best result: {value}";
    }
}
