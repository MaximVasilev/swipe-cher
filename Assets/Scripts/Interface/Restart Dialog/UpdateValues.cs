﻿using UnityEngine;
using Doozy.Engine.UI;

/// <summary>
/// Обновление надписей на диалоговом окне окончания игры
/// </summary>
public class UpdateValues : MonoBehaviour
{
    [SerializeField ] private UIPopup popup;

    /// <summary>
    /// Получение значений и их применение
    /// </summary>
    /// <param name="currentValue">Количество набранных очков за сессию</param>
    /// <param name="recordValue">Рекорд игры</param>
    /// <param name="coinsValue">Количество собранных монет</param>
    /// <param name="costValue">Стоимость воскрешения</param>
    public void GetValues(int currentValue, int recordValue, int coinsValue, int costValue)
    {
        popup.Show();

        // Если получен новый рекорд, так и пишем, иначе выводим лучший результат
        string bestRecord = currentValue >= recordValue ? "This is a new record!" : $"Best killed: {recordValue}";

        popup.Data.SetLabelsTexts(
            $"Killed: {currentValue}",
            bestRecord,
            $"Available: {coinsValue}");

        popup.Data.SetButtonsLabels($"Revive for {costValue} gold");

        // Диактивация кнопки воскрешения, если монет для этого недостаточно
        if (coinsValue < costValue)
        {
            popup.Data.Buttons[0].DisableButton();
        }
        else
        {
            popup.Data.Buttons[0].EnableButton();
        }
    }
}
