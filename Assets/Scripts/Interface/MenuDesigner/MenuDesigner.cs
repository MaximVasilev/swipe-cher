﻿using UnityEngine;
using System.Collections;

public class MenuDesigner : MonoBehaviour
{
    [SerializeField] private SettingsPlayer _settingsPlayer;
    [SerializeField] private SettingsLevel _settingsLevel;
    [SerializeField] private SettingsTouch _settingsTouch;

    private void Awake()
    {
        if(_settingsPlayer == null)
        {
            Debug.LogError("Add _settingsPlayer in MenuDesigner in" + name);
            return;
        }

        if (_settingsLevel == null)
        {
            Debug.LogError("Add _settingsLevel in MenuDesigner in" + name);
            return;
        }

        if (_settingsTouch == null)
        {
            Debug.LogError("Add _settingsTouch in MenuDesigner in" + name);
            return;
        }


#if !UNITY_EDITOR
        var firstOpen = PlayerPrefs.GetInt("FirstOpen", 1);
        if (firstOpen == 1)
        {
            PlayerPrefs.SetInt("FirstOpen", 0);

            PlayerPrefs.SetFloat("PeriodAttack", _settingsPlayer.PeriodAttack);
            PlayerPrefs.SetFloat("SpeedPlayer", _settingsLevel.SpeedPlayer);
            PlayerPrefs.SetFloat("SpeedEnemies", _settingsLevel.SpeedEnemies);
            PlayerPrefs.SetFloat("PeriodSpawnEnemy", _settingsLevel.PeriodSpawnEnemy);
            PlayerPrefs.SetFloat("ForceArrow", _settingsLevel.ForceArrow);
            PlayerPrefs.SetFloat("TimeClick", _settingsTouch.TimeClick);
        }

        _settingsPlayer.PeriodAttack = PlayerPrefs.GetFloat("PeriodAttack", 0.35f);
        _settingsLevel.SpeedPlayer = PlayerPrefs.GetFloat("SpeedPlayer", 3f);
        _settingsLevel.SpeedEnemies = PlayerPrefs.GetFloat("SpeedEnemies",3f);
        _settingsLevel.PeriodSpawnEnemy = PlayerPrefs.GetFloat("PeriodSpawnEnemy", 3f);
        _settingsLevel.ForceArrow = PlayerPrefs.GetFloat("ForceArrow", 15f);
        _settingsTouch.TimeClick = PlayerPrefs.GetFloat("TimeClick", 0.15f);
#endif
    }
}
