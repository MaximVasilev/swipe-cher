﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PeriodBarrels : MonoBehaviour
{
    [SerializeField] private Text _currentScore;
    [SerializeField] private Slider _sliderScore;
    [SerializeField] private SettingsBarrels _settingsBarrels;

    private void Awake()
    {

#if !UNITY_EDITOR
        _settingsBarrels.PeriodOfCreation = PlayerPrefs.GetFloat("PeriodOfCreationBarrels", 4f);
#endif

        SetCurrentScore(_settingsBarrels.PeriodOfCreation);
        _sliderScore.value = _settingsBarrels.PeriodOfCreation;
    }


    private void OnDestroy()
    {
#if !UNITY_EDITOR
         PlayerPrefs.SetFloat("PeriodOfCreationBarrels", _settingsBarrels.PeriodOfCreation);
#endif
    }

    public void SetCurrentScore(float score)
    {
        _currentScore.text = score.ToString();
        _settingsBarrels.PeriodOfCreation = score;
    }
}
