﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SlidersMenuDesigner : MonoBehaviour
{
    [SerializeField] private SettingsPlayer _settingsPlayer;
    [SerializeField] private SettingsLevel _settingsLevel;
    [SerializeField] private SettingsTouch _settingsTouch;

    [Space]

    [SerializeField] private Text _currentPeriodNewShot;
    [SerializeField] private Slider _sliderPeriodNewShot;   

    [Space]

    [SerializeField] private Text _currentPlayersWalkingSpeed;
    [SerializeField] private Slider _sliderPlayersWalkingSpeed;

    [Space]

    [SerializeField] private Text _currentWalkingSpeedBots;
    [SerializeField] private Slider _sliderWalkingSpeedBots;

    [Space]

    [SerializeField] private Text _currentPeriodBetweenSpawnsNewEnemies;
    [SerializeField] private Slider _sliderPeriodBetweenSpawnsNewEnemies;

    [Space]

    [SerializeField] private Text _currentForceArrowsFlight;
    [SerializeField] private Slider _sliderForceArrowsFlight;


    [Space]

    [SerializeField] private Text _currentTimeClick;
    [SerializeField] private Slider _sliderTimeClick;

    private void Awake()
    {

#if !UNITY_EDITOR
        _settingsPlayer.PeriodAttack = PlayerPrefs.GetFloat("PeriodAttack", 0.35f);
        _settingsLevel.SpeedPlayer = PlayerPrefs.GetFloat("SpeedPlayer", 3f);
        _settingsLevel.SpeedEnemies = PlayerPrefs.GetFloat("SpeedEnemies", 3f);
        _settingsLevel.PeriodSpawnEnemy = PlayerPrefs.GetFloat("PeriodSpawnEnemy", 3f);
        _settingsLevel.ForceArrow = PlayerPrefs.GetFloat("ForceArrow", 15f);
        _settingsTouch.TimeClick = PlayerPrefs.GetFloat("TimeClick", 0.15f);
#endif


        SetCurrentPeriodNewShot(_settingsPlayer.PeriodAttack);
        SetCurrentPlayersWalkingSpeed(_settingsLevel.SpeedPlayer);
        SetCurrentWalkingSpeedBots(_settingsLevel.SpeedEnemies);
        SetCurrentPeriodBetweenSpawnsNewEnemies(_settingsLevel.PeriodSpawnEnemy);
        SetCurrentForceArrowsFlight(_settingsLevel.ForceArrow);
        SetCurrentForceArrowsFlight(_settingsLevel.ForceArrow);
        SetCurrentTimeClick(_settingsTouch.TimeClick);

        _sliderPeriodNewShot.value = _settingsPlayer.PeriodAttack;
        _sliderPlayersWalkingSpeed.value = _settingsLevel.SpeedPlayer;
        _sliderWalkingSpeedBots.value = _settingsLevel.SpeedEnemies;
        _sliderPeriodBetweenSpawnsNewEnemies.value = _settingsLevel.PeriodSpawnEnemy;
        _sliderForceArrowsFlight.value = _settingsLevel.ForceArrow;
        _sliderTimeClick.value = _settingsTouch.TimeClick;
    }


    private void OnDestroy()
    {

#if !UNITY_EDITOR
        PlayerPrefs.SetFloat("PeriodAttack", _settingsPlayer.PeriodAttack);
        PlayerPrefs.SetFloat("SpeedPlayer", _settingsLevel.SpeedPlayer);
        PlayerPrefs.SetFloat("SpeedEnemies", _settingsLevel.SpeedEnemies);
        PlayerPrefs.SetFloat("PeriodSpawnEnemy", _settingsLevel.PeriodSpawnEnemy);
        PlayerPrefs.SetFloat("ForceArrow", _settingsLevel.ForceArrow);
        PlayerPrefs.SetFloat("TimeClick", _settingsTouch.TimeClick);
#endif
    }



    public void SetCurrentPeriodNewShot(float value)
    {
        _currentPeriodNewShot.text = value.ToString();
        _settingsPlayer.PeriodAttack = value;
    }

    public void SetCurrentPlayersWalkingSpeed(float value)
    {
        _currentPlayersWalkingSpeed.text = value.ToString();
        _settingsLevel.SpeedPlayer = value;
    }

    public void SetCurrentWalkingSpeedBots(float value)
    {
        _currentWalkingSpeedBots.text = value.ToString();
        _settingsLevel.SpeedEnemies = value;
    }
    public void SetCurrentPeriodBetweenSpawnsNewEnemies(float value)
    {
        _currentPeriodBetweenSpawnsNewEnemies.text = value.ToString();
        _settingsLevel.PeriodSpawnEnemy = value;
    }

    public void SetCurrentForceArrowsFlight(float value)
    {
        _currentForceArrowsFlight.text = value.ToString();
        _settingsLevel.ForceArrow = value;
    }

    public void SetCurrentTimeClick(float value)
    {
        _currentTimeClick.text = value.ToString();
        _settingsTouch.TimeClick = value;
    }

}
