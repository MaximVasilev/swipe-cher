﻿using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Изменение значения, отображаемого на тектовом поле с количеством набранных очков
/// </summary>
public class ChangingScore : MonoBehaviour
{
    [SerializeField] private Text scoreLabel;

    public void SetText(int value)
    {
        scoreLabel.text = value.ToString();
    }
}
