﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

/// <summary>
/// Изменение значения, отображаемого на тектовом поле с количеством набранных монет
/// </summary>
public class ChangingCoin : MonoBehaviour
{
    [SerializeField] private Text coinLabel;

    public void SetText(int value)
    {
        coinLabel.text = value.ToString();
    }
}
