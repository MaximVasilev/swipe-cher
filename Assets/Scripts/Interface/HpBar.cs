﻿using UnityEngine;
using UnityEngine.UI;

public class HpBar : MonoBehaviour
{
    [SerializeField] private Image _hp;

    private float _maxHp = 100f;

    public void SetHp(float value)
    {
        _hp.fillAmount = value / _maxHp;
    }
}
