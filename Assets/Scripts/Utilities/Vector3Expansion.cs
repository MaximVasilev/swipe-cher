﻿using UnityEngine;

public static class Vector3Expansion
{
    public static Vector3 ClampMagnitude(this Vector3 vector, float minLength, float maxLength)
    { 
        double sqrMagnitude = vector.sqrMagnitude;

        if (sqrMagnitude > (double) maxLength * maxLength) 
            return vector.normalized * maxLength;

        else if (sqrMagnitude < (double) minLength * minLength) 
            return vector.normalized * minLength;

        return vector;
    }
}

