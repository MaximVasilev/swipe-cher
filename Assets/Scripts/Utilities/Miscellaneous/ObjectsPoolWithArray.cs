using UnityEngine;
using System.Collections.Generic;

namespace Utilities
{
    /// <summary>
    /// Пул игровых объектов, может быть использован для любых GameObject (измененная версия из Gamebase, чтобы можно было работать не с одним префабом, 
    /// а наполнять пул рандомными варианты из коллекции)
    /// </summary>
    public class ObjectsPoolWithArray
    {
        /// <summary>
        /// Количество объектов в пуле на данный момент
        /// </summary>
        public int Count { get; private set; }

        /// <summary>
        /// Должен ли пул автоматически расширяться если нет достаточного количества объектов
        /// </summary>
        public bool Expandable { get; }

        /// <summary>
        /// Объект-родитель в иерархии сцены
        /// </summary>
        public Transform ParentObject { get; }

        /// <summary>
        /// Префаб, которым будет заполнен пул объектов, при инициализации
        /// </summary>
        readonly GameObject prefabObject;

        /// <summary>
        /// Пул объектов
        /// </summary>
        readonly List<GameObject> objectsList;

        /// <summary>
        /// Конструктор, инициализирует пул, заполняет его префабами в нужном количестве, и определяет для каждого объекта родителя в иерархии
        /// </summary>
        /// <param name="prefab">Массив префабов создаваемых объектов</param>
        /// <param name="initCount">Количество объектов, создаваемых при инициализации пула</param>
        /// <param name="expandable">Расширяемый ли пул? Если да, то при необходимости будет автоматически создавать новые объекты</param>
        /// <param name="parentTransform">Родительский объект в иерархии для пула</param>
        /// <param name="objectName">Имя создаваемых объектов по умолчанию</param>
        public ObjectsPoolWithArray(GameObject[] prefab, int initCount, Transform parentTransform, bool expandable = false, string objectName = "")
        {
            objectsList = new List<GameObject>();
            ParentObject = parentTransform;
            Expandable = expandable;

            for (int i = 0; i < initCount; i++)
            {
                prefabObject = prefab[Random.Range(0, prefab.Length)];
                Add(prefabObject, objectName);
            }
        }

        /// <summary>
        /// Индексатор, возвращает выбраный объект пула
        /// </summary>
        /// <param name="index">Индекс требуемого объекта</param>
        /// <returns></returns>
        public GameObject this[int index]
        {
            get { return objectsList[index]; }
            set { objectsList[index] = value; }
        }

        /// <summary>
        /// Метод добавляет новый объект в пул объектов
        /// </summary>
        /// <param name="prefab">Префаб создаваемого объекта</param>
        /// <param name="objectName">Имя для создаваемого объекта</param>
        /// <returns></returns>
        private GameObject Add(GameObject prefab, string objectName = "")
        {
            GameObject newObject = Object.Instantiate(prefab, ParentObject);
            if (!string.IsNullOrEmpty(objectName))
                newObject.name = $"{objectName}_{Count.ToString()}";

            newObject.SetActive(false);
            objectsList.Add(newObject);
            Count++;
            return newObject;
        }

        /// <summary>
        /// Метод получения из пула объекта, находит неактивный объект, активирует его и возвращает. Если нет подходящего объекта, и пул нерасширяемый - возвращает null
        /// </summary>
        /// <param name="autoActivate">Необходимо ли автоматически активировать объект</param>
        /// <returns></returns>
        public GameObject GetObject(bool autoActivate = true)
        {
            if (objectsList.Count != 0)
            {
                for (var index = 0; index < objectsList.Count; index++)
                {
                    var candidate = objectsList[index];
                    if (candidate.activeInHierarchy) continue;

                    //Если необходимо автоматически активировать объекты при запросе - активируем.
                    if (autoActivate)
                        candidate.SetActive(true);
                    return candidate;
                }
            }

            //Если не найден неактивный объект
            if (Expandable)
            {
                //Если все объекты используются или пул пуст, создает новый объект, добавляет его в пул, и возвращает его.
                GameObject newObject = Add(prefabObject);
                //Если необходимо автоматически активировать объекты при запросе - активируем.
                if (autoActivate)
                    newObject.SetActive(true);
                return newObject;
            }

            return null;
        }
    }
}
