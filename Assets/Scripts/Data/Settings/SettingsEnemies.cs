﻿using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;
using System.Linq;

[CreateAssetMenu(menuName = "Log Jumping/Enemies Settings")]
public class SettingsEnemies : ScriptableObject
{

    [Title("Настройка генерации врагов")]

    [InfoBox("Префабы создаваемых врагов")]
    [ValidateInput("CheckingAssetList", "Должен быть задан хотя бы один префаб", InfoMessageType.Error)]
    [AssetsOnly]
    public List<GameObject> Enemies;

    [Space]

    [InfoBox("Размер зоны генерации по оси X (в процентах от ширины экрана)")]
    [Range(20f, 100f)]
    public float FieldWidth = 60f;

    [Space]

    [InfoBox("Период генерации врагов (количество секунд между созданием новых экземпляров)")]
    [ValidateInput("CheckingPeriodOfCreation", "При установке значения 0 враги не будут генерироваться", InfoMessageType.Warning)]
    [Range(0f, 15f)]
    public float PeriodOfCreation = 3f;


    private bool CheckingAssetList(List<GameObject> AssetList)
    {
        return AssetList.Count != 0;
    }

    private bool CheckingPeriodOfCreation(float PeriodOfCreation)
    {
        return PeriodOfCreation != 0;
    }
}
