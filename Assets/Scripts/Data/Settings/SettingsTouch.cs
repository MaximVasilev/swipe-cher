﻿using UnityEngine;
using Sirenix.OdinInspector;

[CreateAssetMenu(menuName = "SwipeCher/Touch Settings")]
public class SettingsTouch : ScriptableObject
{
    [InfoBox("Максимальная длительность тапа")]
    [Range(0f, 1f)]
    public float TimeClick = 0.1f;
}