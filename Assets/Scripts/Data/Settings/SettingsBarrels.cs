﻿using UnityEngine;
using UnityEditor;
using Sirenix.OdinInspector;

[CreateAssetMenu(menuName = "Log Jumping/Barrels Settings")]
public class SettingsBarrels : ScriptableObject
{

    [Title("Настройка генерации бочек")]

    [InfoBox("Период генерации бочек (количество секунд между созданием новых экземпляров)")]
    [ValidateInput("CheckingPeriodOfCreation", "При установке значения 0 бочки не будут генерироваться", InfoMessageType.Warning)]
    [Range(0f, 10f)]
    public float PeriodOfCreation = 4f;

    private bool CheckingPeriodOfCreation(float PeriodOfCreation)
    {
        return PeriodOfCreation != 0;
    }
}