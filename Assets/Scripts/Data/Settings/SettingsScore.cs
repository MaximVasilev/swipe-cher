﻿using UnityEngine;
using Sirenix.OdinInspector;

[CreateAssetMenu(menuName = "Log Jumping/Score Settings")]
public class SettingsScore : ScriptableObject
{
    [Title("Настройка скорости набора очков")]

    [InfoBox("Скорость набора очков (единиц в секунду)")]
    [ValidateInput("CheckingSpeedScoring", "Значение должно быть положительным", InfoMessageType.Error)]
    public int SpeedScoring = 10;


    private bool CheckingSpeedScoring(int SpeedScoring)
    {
        this.SpeedScoring = Mathf.Clamp(SpeedScoring, 0, SpeedScoring);
        return SpeedScoring > 0;
    }

}
