﻿using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;
using System.Linq;

[CreateAssetMenu(menuName = "Log Jumping/Branches Settings")]
public class SettingsBranches : ScriptableObject
{
    
    [Title("Настройка генерации бревен")]

    [InfoBox("Префабы создаваемых бревен")]
    [ValidateInput("CheckingAssetList", "Должен быть задан хотя бы один префаб", InfoMessageType.Error)]
    [AssetsOnly]
    public List<GameObject> Branches;

    [Space]

    [InfoBox("Размер зоны генерации по оси X (в процентах от ширины экрана)")]
    [Range(20f, 100f)]
    public float FieldWidth = 60f;

    [Space]

    [InfoBox("Период генерации бревен (количество секунд между созданием новых экземпляров)")]
    [ValidateInput("CheckingPeriodOfCreation", "При установке значения 0 бревна не будут генерироваться", InfoMessageType.Warning)]
    [Range(0f, 2f)]
    public float PeriodOfCreation = 0.3f;


    private bool CheckingAssetList(List<GameObject> AssetList)
    {
        return AssetList.Count != 0;
    }

    private bool CheckingPeriodOfCreation(float PeriodOfCreation)
    {
        return PeriodOfCreation != 0;
    }
}
