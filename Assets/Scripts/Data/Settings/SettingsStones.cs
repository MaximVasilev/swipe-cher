﻿using UnityEngine;
using System.Collections;
using Sirenix.OdinInspector;

[CreateAssetMenu(menuName = "Log Jumping/Stones Settings")]
public class SettingsStones : ScriptableObject
{

    [Title("Настройка генерации камней")]

    [InfoBox("Период генерации камней (количество секунд между созданием новых экземпляров)")]
    [ValidateInput("CheckingPeriodOfCreation", "При установке значения 0 камни не будут генерироваться", InfoMessageType.Warning)]
    [Range(0f, 10f)]
    public float PeriodOfCreation = 5f;

    private bool CheckingPeriodOfCreation(float PeriodOfCreation)
    {
        return PeriodOfCreation != 0;
    }
}
