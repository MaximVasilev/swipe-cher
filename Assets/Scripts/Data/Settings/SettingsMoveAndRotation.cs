﻿using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

[CreateAssetMenu(menuName = "Log Jumping/Move and rotation Settings")]
public class SettingsMoveAndRotation : ScriptableObject
{
    [Title("Настройка движения")]

    [InfoBox("Диапазон скорости движения объекта по течению")]
    [InfoBox("При установке скорости большей либо не намного меньшей, чем скорость движения игрока в прыжке, прыжки будут выполняться некорректно (игрок будет зависать в воздухе в попытках догнать объект)", InfoMessageType.Warning)]
    [MinMaxSlider(0.5f, 10f, true)]
    public Vector2 MoveSpeed = new Vector2(3f, 4f);

    [Space]

    [InfoBox("Диапазон скорости вращения объектов вокруг оси Y (положительное число - вращение по часовой стрелке, отрицательное - против часовой стрелки)")]
    [MinMaxSlider(-5f, 5f, true)]
    public Vector2 RotationSpeed = new Vector2(-0.1f, 0.1f);

}
