﻿using UnityEngine;
using Sirenix.OdinInspector;

[CreateAssetMenu(menuName = "SwipeCher/Player Settings")]
public class SettingsPlayer : ScriptableObject
{
    [InfoBox("Максимальное здоровье")]
    [Range(1f, 500f)]
    public float MaxHealth = 100f;

    [Space]

    [InfoBox("Период новой атаки")]
    [Range(0.3f, 5f)]
    public float PeriodAttack = 1f;

    [Space]

    [InfoBox("Скорость восстановления здоровья")]
    [Range(0f, 1f)]
    public float SpeedRegen = 0.6f;
}