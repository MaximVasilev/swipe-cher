﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

[CreateAssetMenu(menuName = "Log Jumping/Coins Settings")]
public class SettingsCoins : ScriptableObject
{
    [Title("Настройка генерации монет")]

    [InfoBox("Префаб создаваемых монет")]
    [ValidateInput("CheckingCoinPrefab", "Должен быть задан префаб", InfoMessageType.Error)]
    [AssetSelector]
    public GameObject CoinPrefab;

    [Space]

    [InfoBox("Процент бревен, на которых будут появляться монеты")]
    [Range(0, 100)]
    public int ProbabilityCreatingCoin = 20;

    [Space]

    [InfoBox("Стоимость рестарта игры в монетах")]
    [ValidateInput("CheckingCostRestart", "Значение должно быть положительным (если не нужно доплачивать игроку за рестарт)", InfoMessageType.Error)]
    public int CostRestart = 10;


    private bool CheckingCoinPrefab(GameObject CoinPrefab)
    {
        return CoinPrefab != null;
    }

    private bool CheckingCostRestart(int CostRestart)
    {
        return CostRestart >= 0;
    }
}
