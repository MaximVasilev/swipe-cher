﻿using UnityEngine;
using Sirenix.OdinInspector;

[CreateAssetMenu(menuName = "SwipeCher/Level Settings")]
public class SettingsLevel : ScriptableObject
{
    [InfoBox("Скорость игрока")]
    [Range(1f, 15f)]
    public float SpeedPlayer = 7f;

    [Space]

    [InfoBox("Скорость врагов")]
    [Range(1f, 15f)]
    public float SpeedEnemies = 7f;

    [Space]

    [InfoBox("Скорость полета стрелы")]
    [Range(1f, 100f)]
    public float ForceArrow = 30f;

    [Space]

    [InfoBox("Период нового спауна врага")]
    [Range(1f, 10f)]
    public float PeriodSpawnEnemy = 3f;

    [Space]

    [InfoBox("Период нового спауна препятствия")]
    [Range(1f, 10f)]
    public float PeriodSpawnItems = 1f;
}