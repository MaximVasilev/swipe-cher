﻿using Gamebase.Systems.Resources;

/// <summary>
/// Класс, хранящий и изменяющий количество набранных очков за сессию
/// </summary>
public class ScoreResources : Resource
{
    private int currentValue;
    
    public override void Add(int count)
    {
        if (count > 0)
        {
            currentValue += count;
        }
    }

    public override int GetCurrentValue()
    {
        return currentValue;
    }

    public override void GetSavedValue()
    {
        currentValue = 0;
    }

    public override void Set(int count)
    {
        currentValue = count;
    }
}
