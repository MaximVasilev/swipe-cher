﻿using UnityEngine;
using Gamebase.Systems.Resources;

/// <summary>
/// Класс, хранящий и изменяющий значение лучшего результата
/// </summary>
public class BestScoreResources : Resource
{
    private int currentValue;
    private int record;
    private const string KEY = "BestScore";
    
    public override void Add(int count)
    {
        if (count > 0)
        {
            currentValue += count;
            Save(count);
        }
    }

    public override int GetCurrentValue()
    {
        return record;
    }

    public override void GetSavedValue()
    {
        currentValue = 0;
        record = PlayerPrefs.GetInt(KEY, 0);
    }

    public override void Set(int count)
    {
        currentValue = count;
        Save(count);
    }

    private void Save(int value)
    {
        if (currentValue > record)
        {
            record = currentValue;
            PlayerPrefs.SetInt(KEY, currentValue);
        }
    }
}
