﻿using UnityEngine;
using Gamebase.Systems.Resources;

/// <summary>
/// Класс, хранящий и изменяющий количество набранных монет
/// </summary>
public class CoinsResources : Resource
{
    private int currentValue;
    private const string KEY = "Coins";

    public override void Add(int count)
    {
        currentValue += count;
        Save(count);
    }

    public override int GetCurrentValue()
    {
        return currentValue;
    }

    public override void GetSavedValue()
    {
        currentValue = PlayerPrefs.GetInt(KEY, 0);
    }

    public override void Set(int count)
    {
        currentValue = count;
        Save(count);
    }

    private void Save(int value)
    {
        PlayerPrefs.SetInt(KEY, value);
    }
}
