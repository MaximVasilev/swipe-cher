using System.Collections.Generic;
using UnityEngine;

namespace Gamebase.Systems.Levels
{
    public abstract class LevelProgression : ScriptableObject
    {
        public List<LevelConfiguration> levels;
    }
}