using UnityEngine;

namespace Gamebase.Systems.Levels
{
    public abstract class LevelConfiguration : ScriptableObject
    {
        [TextArea]
        public string Description;
    }
}