using System.Collections;
using Sirenix.OdinInspector;
using UnityEngine;

namespace Gamebase.Systems.Levels
{
    /// <summary>
    /// Система уровней. Предоставляет интерфейс для загрузки уровней (необходимо имплементировать). По умолчанию система использует класс настроек прогресси уровней LevelProgression. Для конфигурации уровня изменяйте класс LevelConfiguration
    /// </summary>
    [TypeInfoBox("Система уровней. Предоставляет интерфейс для загрузки уровней (необходимо имплементировать). По умолчанию система использует класс настроек прогресси уровней LevelProgression. Для конфигурации уровня изменяйте класс LevelConfiguration")]
    [DisallowMultipleComponent]
    public abstract class LevelsSystem : MonoBehaviour
    {
        [SerializeField] private LevelProgression levelProgression;

        public static LevelsSystem instance;
        private void Awake()
        {
            instance = this;
        }

        public void LoadLevelByIndex(int levelIndex)
        {
            LoadLevel(levelProgression.levels[levelIndex]);
        }

        public abstract void LoadLevel(LevelConfiguration configuration);
    }
}