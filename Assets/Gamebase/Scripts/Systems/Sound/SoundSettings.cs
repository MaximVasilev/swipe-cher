﻿using Sirenix.OdinInspector;
using System;
using System.Linq;
using Gamebase.Miscellaneous;
using Gamebase.Systems.Debugging;
#if UNITY_EDITOR
using UnityEditor;
#endif
using UnityEngine;

namespace Gamebase.Systems.Sound
{
    [Serializable]
    public class SoundElement
    {
        [Delayed, Tooltip("Название звукового эффекта (на английском, без пробелов и специальных символов)")]
        public string name = "";
        [Tooltip("Звуковой клип(клипы)")]
        public AudioClip[] clips;
        [Delayed, Tooltip("Громкость звука")]
        [Range(0f, 1f)]
        public float volume = 1f;
    }

    [Serializable]
    public class MusicElement
    {
        [Delayed, Tooltip("Название музыки (на английском, без пробелов и специальных символов)")]
        public string name = "";
        [Tooltip("Звуковой клип")]
        public AudioClip musicClip;
    }

    /// <summary>
    /// Содержит настройки звуковой системы
    /// </summary>
    [CreateAssetMenu(menuName = "Gamebase/SoundSettings")]
    public class SoundSettings : ScriptableObject
    {
        [Tooltip("Минимальная задержка между двумя звуками")]
        public float minFXLatency = 0.1f;
        [Tooltip("Максимальная задержка между двумя звуками. Если запрос на звук был в момент времени Х, а его очередь наступила только к Х+maxFXLatency, то звук воспроизведен не будет")]
        public float maxFXLatency = 0.5f;
        [Tooltip("Длительность фейда затухания музыки")]
        public float musicFadeOutLength = 1.5f;
        [Tooltip("Минимальное значение громкости музыки на котором заканчивается фейд и включается следующая композиция")]
        public float minFadeVolumeMultiplier = 0.2f;

        [Tooltip("Звуковые эффекты"), OnValueChanged("RebuildEnumCode", includeChildren: true)]
        [SerializeField] SoundElement[] sounds;
        [Tooltip("Музыкальные композиции"), OnValueChanged("RebuildEnumCode", includeChildren: true)]
        [SerializeField] MusicElement[] music;

        public SoundElement GetSoundElement(Sound soundType)
        {
            for (int i = 0; i < sounds.Length; i++)
            {
                if (sounds[i].GetHashCode() == soundType.GetHashCode())
                    return sounds[i];
            }
            DebugSystem.LogError($"SoundSettings - Не найден звук типа {soundType}!");
            return null;
        }

        public MusicElement GetMusicElement(Music musicType)
        {
            for (int i = 0; i < music.Length; i++)
            {
                if (music[i].GetHashCode() == musicType.GetHashCode())
                    return music[i];
            }
            DebugSystem.LogError($"SoundSettings - Не найдена музыка типа {musicType}!");
            return null;
        }

#if UNITY_EDITOR
        [ContextMenu("Manual Rebuild Enum Code")]
        /// <summary>
        /// Пересборка файла с перечислением
        /// </summary>
        public void RebuildEnumCode()
        {
            if (sounds != null)
                DynamicEnums.ReplaceAndSave("SoundEnum", sounds.Select(e => e.name).ToList());
            if (music != null)
                DynamicEnums.ReplaceAndSave("MusicEnum", music.Select(e => e.name).ToList());
        }

#endif
    }
}