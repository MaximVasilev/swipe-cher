﻿using System.Collections;
using UnityEngine;
using System.Collections.Generic;
using System.Linq;
using Gamebase.Miscellaneous;
using Sirenix.OdinInspector;

namespace Gamebase.Systems.Sound
{
    public class SoundWithTime
    {
        public Sound sound;
        public float time;
        public bool unscaled;

        public SoundWithTime(Sound sound, float time)
        {
            this.sound = sound;
            this.time = time;
        }

        public SoundWithTime(Sound sound)
        {
            this.sound = sound;
            this.unscaled = true;
        }
    }

    /// <summary>
    /// Система звуков позволяет хранить все 2D звуковые эффекты и музыку с настройками в одном конфигурационном файле, а также предоставляет интерфейс для их воспроизведения, остановки и смешивания. Также система позволяет контролировать минимальную и максимальную задержки между воспроизведением двух вызванных одновременно звуковых эффектов.
    /// </summary>
    [TypeInfoBox("Система звуков позволяет хранить все 2D звуковые эффекты и музыку с настройками в одном конфигурационном файле, а также предоставляет интерфейс для их воспроизведения, остановки и смешивания. Также система позволяет контролировать минимальную и максимальную задержки между воспроизведением двух вызванных одновременно звуковых эффектов.")]
    [AddComponentMenu("Gamebase/Sound System")]
    public class SoundSystem : Singleton<SoundSystem>
    {
        readonly Queue<SoundWithTime> soundsQueue = new Queue<SoundWithTime>();
        private bool someSoundIsPlaying;

//         public static SoundSystem instance;
//         private void Awake()
//         {
//             if (instance == null)
//             {
//                 instance = this;
//                 DontDestroyOnLoad(gameObject);
//             }
//             else
//             {
//                 DestroyImmediate(this);
//             }
//         }
// 
//         [SerializeField]
//         private bool AutoInitializeOnStart = true;
// 
//         private void Start()
//         {
//             if (AutoInitializeOnStart)
//                 Initialize();
//         }

        public override void Initialize()
        {
            musicLevelVolume = musicSource.volume;

            //Если это первый запуск игры - включаем музыку и звуки, сохраняем настройки
            if (!PlayerPrefs.HasKey("MusicOn"))
                PlayerPrefs.SetInt("MusicOn", 1);
            if (!PlayerPrefs.HasKey("SoundOn"))
                PlayerPrefs.SetInt("SoundOn", 1);

            //Загружаем игровые межсесионные настройки
            MusicOn = PlayerPrefs.GetInt("MusicOn", 0) == 1;
            SoundOn = PlayerPrefs.GetInt("SoundOn", 0) == 1;
        }

        [SerializeField]
        AudioSource FXSource;
        [SerializeField]
        AudioSource ComboSequenceFxSource;
        [SerializeField]
        AudioSource musicSource;

        /// <summary>
        /// Настройки содержащие параметры и ссылки на все звуки
        /// </summary>
        [SerializeField] SoundSettings settings;

        private bool soundOn;
        public bool SoundOn
        {
            get { return soundOn; }
            set
            {
                soundOn = value;
            }
        }

        private float musicLevelVolume;//Начальная громкость музыки

        private bool musicOn;
        public bool MusicOn
        {
            get { return musicOn; }
            set
            {
                if (value != musicOn)
                    musicSource.time = 0f;

                musicSource.volume = value ? musicLevelVolume : 0f;
                musicOn = value;
            }
        }


        private SoundWithTime workingSound;
        private float timer;
        private void Update()
        {
            if (soundsQueue.Count > 0)
            {
                timer += Time.unscaledDeltaTime;
                if (timer >= settings.minFXLatency)
                {
                    workingSound = soundsQueue.Dequeue();
                    timer = 0f;
                    if (workingSound.unscaled || Time.unscaledTime - workingSound.time <= settings.maxFXLatency)
                    {
                        SoundElement sound = settings.GetSoundElement(workingSound.sound);
                        AudioClip clip = sound.clips[Random.Range(0, sound.clips.Length)];
                        FXSource.PlayOneShot(clip, sound.volume);
                    }
                }
            }
        }

        public void PlaySoundUnscaled(Sound sound)
        {
            if (!SoundOn) return;

            soundsQueue.Enqueue(new SoundWithTime(sound));
        }


        public void PlaySound(Sound sound)
        {
            if (!SoundOn) return;

            soundsQueue.Enqueue(new SoundWithTime(sound, Time.unscaledTime));
        }

        private Music playingMusic = Music.None;
        public void PlayMusic(Music music, bool loop = true)
        {
            if (music == playingMusic) //Если данная музыка уже проигрывается
                return;
            StartCoroutine(PlayMusicCoroutine(music, loop));
        }

        /// <summary>
        /// Запускает две дорожки одновременно
        /// </summary>
        public void PlayMusic(Music music, Music anotherMusic, bool loop = true, bool fadePrevious = true)
        {
            if (music == playingMusic || music == anotherMusic) //Если данная музыка уже проигрывается
                return;
            StartCoroutine(PlayMusicCoroutine(music, anotherMusic, loop, fadePrevious));
        }

        /// <summary>
        /// Запускает один трек
        /// </summary>
        IEnumerator PlayMusicCoroutine(Music music, bool loop = true)
        {
            playingMusic = music;
            if (musicSource.isPlaying)
                yield return StartCoroutine(FadeCoroutine(musicSource, settings.musicFadeOutLength));

            StopMusic(musicSource);

            musicSource.clip = settings.GetMusicElement(music).musicClip;
            musicSource.loop = loop;
            musicSource.Play();
        }

        /// <summary>
        /// Запускает два трека (первый трек будет считаться основным во всех условиях playingMusic)
        /// </summary>
        IEnumerator PlayMusicCoroutine(Music music, Music anotherMusic, bool loop = true, bool fadePrevious = true)
        {
            playingMusic = music;
            var sources = GetAllAudioSources(); //Получаем все AudioSource компоненты системы
            var secondMusicSource = sources.Length < 2 ? musicSource.gameObject.AddComponent<AudioSource>() : sources[1]; //Создаем второй AudioSource, если ещё не имеем

            //Настраиваем второй audioSource по основному
            secondMusicSource.bypassEffects = musicSource.bypassEffects;
            secondMusicSource.bypassListenerEffects = musicSource.bypassListenerEffects;
            secondMusicSource.bypassReverbZones = musicSource.bypassReverbZones;
            secondMusicSource.playOnAwake = musicSource.playOnAwake;
            secondMusicSource.volume = musicSource.volume;


            if (fadePrevious) //Необходимо ли заглушать предыдущую дорожку перед отключением
                if (musicSource.isPlaying)
                    yield return StartCoroutine(FadeCoroutine(musicSource, settings.musicFadeOutLength));

            StopMusic(musicSource);

            musicSource.clip = settings.GetMusicElement(music).musicClip;
            musicSource.loop = loop;
            musicSource.Play();

            secondMusicSource.clip = settings.GetMusicElement(anotherMusic).musicClip;
            secondMusicSource.loop = loop;
            secondMusicSource.Play();
        }

        public void FadeOutMusic()
        {
            StartCoroutine(FadeCoroutine(musicSource, settings.musicFadeOutLength));
        }

        /// <summary>
        /// Отключает с затуханием выбранную музыку (если она включена). Полезно при одновременном запуске двух дорожек, чтобы отключить неосновную дорожку
        /// </summary>
        public void FadeOutMusic(Music music)
        {
            var sources = GetAllAudioSources();
            var source = sources.FirstOrDefault(x => x.clip == settings.GetMusicElement(music).musicClip);
            StartCoroutine(FadeCoroutine(source, settings.musicFadeOutLength));
        }


        private bool isFade;

        IEnumerator FadeCoroutine(AudioSource source, float time)
        {
            if (isFade || source == null) yield break;

            isFade = true;
            float startVolume = musicSource.volume;
            float start = startVolume;
            float end = startVolume * settings.minFadeVolumeMultiplier;
            float i = 0;
            float step = 1f / time;

            while (i <= 1.0)
            {
                i += step * Time.unscaledDeltaTime;
                source.volume = Mathf.Lerp(start, end, i);
                yield return new WaitForSecondsRealtime(0.04f);
            }

            source.volume = startVolume;
            isFade = false;
            StopMusic(source);
        }

        AudioSource SourceFromMusic(Music music)
        {
            return musicSource;
        }

        /// <summary>
        /// Проиграть звук повышающийся по тону, самый низкий (стандартной высоты) будет при параметре равном 0,
        /// затем при повышении параметра на 1 звук будет повышаться на полтона
        /// </summary>
        /// <param name="semitoneCount"></param>
        public void PlayComboSequenceSound(Sound type, int semitoneCount)
        {
            if (!SoundOn) return;

            ComboSequenceFxSource.pitch = Mathf.Pow(1.05946f, (semitoneCount) * 1);
            SoundElement sound = settings.GetSoundElement(type);
            AudioClip clip = sound.clips[Random.Range(0, sound.clips.Length)];

            ComboSequenceFxSource.PlayOneShot(clip, sound.volume);
        }

        /// <summary>
        /// Отключает основную дорожку
        /// </summary>
        public void StopMusic()
        {
            musicSource.Stop();
        }

        /// <summary>
        /// Отключает выбранную дорожку (если она есть)
        /// </summary>
        public void StopMusic(AudioSource source)
        {
            if (source != null)
                source.Stop();
        }

        private AudioSource[] GetAllAudioSources()
        {
            var sources = musicSource.gameObject.GetComponents<AudioSource>();
            return sources;
        }
        
    }
}