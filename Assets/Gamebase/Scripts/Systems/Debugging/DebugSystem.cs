using Gamebase.Miscellaneous;
using Sirenix.OdinInspector;
using UnityEngine;


namespace Gamebase.Systems.Debugging
{
    /// <summary>
    /// Система для централизованного управления отладкой. В настройках системы можно установить фильтры для сообщений которые можно пропускать, или которые нужно игнорировать. В коде необходимо использовать методы логгирования DebugSystem вместо Debug
    /// </summary>
    [AddComponentMenu("Gamebase/Debug System")]
    [TypeInfoBox("Система для централизованного управления отладкой. В настройках системы можно установить фильтры для сообщений которые можно пропускать, или которые нужно игнорировать. В коде необходимо использовать методы логгирования DebugSystem вместо Debug.")]
    [DisallowMultipleComponent]
    public class DebugSystem : Singleton<DebugSystem>
    {
        private const string prefixString = "DEBUG : ";
        public static DebugSettings settings;
        [Tooltip("Ссылка на файл настроек системы отладки")]
        [SerializeField] DebugSettings Settings;

        public override void Initialize()
        {
            settings = Settings;
        }

        /// <summary>
        /// Предупреждение в консоль
        /// </summary>
        /// <param name="message"></param>
        public static void LogWarning(object message)
        {
            Log(message, isWarning: true);
        }

        /// <summary>
        /// Ошибка в консоль
        /// </summary>
        /// <param name="message"></param>
        public static void LogError(object message)
        {
            UnityEngine.Debug.LogError(message);
        }


        /// <summary>
        /// Сообщение в консоль
        /// </summary>
        /// <param name="message"></param>
        /// <param name="isWarning"></param>
        public static void Log(object message, bool isWarning = false)
        {
            if (settings == null)
            {
                Debug.LogWarning("DebugSystem : settings are null! Using standart UnityEngine.Debug");
                if (isWarning)
                    Debug.LogWarning(message);
                else
                    Debug.Log(message);

                return;
            }

            if (!settings.enableConsole) return;
            if (message is string msg)
            {
                bool accept = false;

                if (settings.enableFilter)
                {
                    foreach (var keyword in settings.keywords)
                    {
                        if (keyword.enabled && !string.IsNullOrEmpty(keyword.key) && msg.Contains(keyword.key))
                        {
                            accept = true;
                            break;
                        }
                    }
                }
                else accept = true;

                if (settings.enableExceptFilter)
                {
                    foreach (var exceptWord in settings.consoleExceptWords)
                    {
                        if (!string.IsNullOrEmpty(exceptWord) && msg.Contains(exceptWord))
                        {
                            accept = false;
                            break;
                        }
                    }
                }

                if (accept)
                {
                    message = string.Concat(prefixString, message);

                    if (isWarning)
                        UnityEngine.Debug.LogWarning(message);
                    else
                        UnityEngine.Debug.Log(message);
                }
            }
            else
            {
                message = string.Concat(prefixString, message);

                if (isWarning)
                {
                    UnityEngine.Debug.LogWarning(message);
                }
                else
                    UnityEngine.Debug.Log(message);
            }
        }


    }
}