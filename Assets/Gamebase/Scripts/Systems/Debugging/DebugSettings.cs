﻿using System;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine;

namespace Gamebase.Systems.Debugging
{

    [Serializable, InlineProperty]
    public class ConsoleModeElement
    {
        [Tooltip("Использовать ли данное ключевое слово в текущем фильтре")]
        [HorizontalGroup(width:50f),HideLabel]
        public bool enabled;
        [Tooltip("Ключевое слово (с учётом регистра)")]
        [HorizontalGroup, HideLabel]
        public string key;
    }

    /// <summary>
    /// Содержит настройки отладки
    /// </summary>
    [CreateAssetMenu(menuName = "Gamebase/Debug Settings")]
    public class DebugSettings : ScriptableObject
    {
        [Tooltip("Включить логгирование")]
        public bool enableConsole;
        [InfoBox("В логи будут попадать только те сообщения, которые содержат что-то из списка ключевых слов, убедитесь, что нужные ключевые слова отмечены как активные.",visibleIfMemberName: "enableFilter", infoMessageType: InfoMessageType.Warning)]
        [Tooltip("Фильтровать ли сообщения (пропускать только те, что содержат что-то из списка ключевых слов)")]
        public bool enableFilter;
        [ShowIf("enableFilter"),Tooltip("Ключевые слова, сообщения которые их содержат будут пропущены в логи")]
        public List<ConsoleModeElement> keywords;
        [InfoBox("В логи НЕ будут попадать те сообщения, которые содержат что-то из списка слов-исключений", visibleIfMemberName: "enableExceptFilter",infoMessageType:InfoMessageType.Warning)]
        [Tooltip("Фильтровать ли сообщения (не пропускать те, что содержат что-то из списка слов-исключений)")]
        public bool enableExceptFilter;
        [ShowIf("enableExceptFilter"), Tooltip("Сообщения с какими словами необходимо пропускать и не записывать в логи")]
        public List<string> consoleExceptWords;
    }
}