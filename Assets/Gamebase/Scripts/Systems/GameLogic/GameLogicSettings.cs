﻿using System;
using System.Collections.Generic;
using System.Linq;
using Gamebase.Systems.GlobalEvents;
using Gamebase.Systems.Resources;
using Gamebase.Systems.Sound;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.Events;

namespace Gamebase.Systems.GameLogic
{
    /// <summary>
    /// Настройки системы игровой логики
    /// </summary>
    public class GameLogicSettings : ScriptableObject
    {
        /// <summary>
        /// Список игровых правил (привязки игровых событий к действиям разных видов)
        /// </summary>
        public List<GlobalEventBinding> eventBindings;
    }

    [Serializable]
    public class GlobalEventBinding
    {
        [Tooltip("Тип глобального события (Из GlobalEventsSystem)")]
        [ValidateInput("ValidateInvokeTypeEvent", "Нельзя выбирать один и тот же тип, в качестве вызывающего и вызываемого события!",ContinuousValidationCheck = true)]
        public GlobalEventType globalEventType;
        [Tooltip("Список действий, которые необходимо выполнять когда происходит указанное событие")]
        public List<GlobalEventBindingAction> actions = new List<GlobalEventBindingAction>();

        bool ValidateInvokeTypeEvent(GlobalEventType eventType)
        {
            return actions.Count(e => e.invokeEventType==eventType && e.type==GlobalEventBindingType.InvokeGlobalEvent)==0;
        }
    }
    /// <summary>
    /// Класс действия игровой логики. Можно например проигрывать звук, музыку, добавлять ресурс, писать сообщение в лог, вызывать глобальнео событие
    /// </summary>
    [Serializable]
    public class GlobalEventBindingAction
    {
        [Tooltip("Тип действия")]
        public GlobalEventBindingType type;
        [ShowIf("@this.type == GlobalEventBindingType.PlaySound")]
        [Tooltip("Тип звука, который необходимо воспроизвести")]
        public Sound.Sound sound;
        [ShowIf("@this.type == GlobalEventBindingType.PlayMusic")]
        [Tooltip("Тип музыки, которую необходимо воспроизвести")]
        public Music music;
        [ShowIf("@this.type == GlobalEventBindingType.AddResource || this.type == GlobalEventBindingType.SetResource")]
        [Tooltip("Ресурс, который необходимо добавить или установить")]
        public ResourceValue resourceValue;
        [ShowIf("@this.type == GlobalEventBindingType.DebugMessage")]
        [Tooltip("Сообщение в логи")]
        public string debugMessage;
        [Tooltip("Тип вызываемого глобального события")]
        [ShowIf("@this.type == GlobalEventBindingType.InvokeGlobalEvent")]
        public GlobalEventType invokeEventType;
    }

    /// <summary>
    /// Тип действия игровой логики
    /// </summary>
    public enum GlobalEventBindingType
    {
        None,
        PlaySound,
        PlayMusic,
        AddResource,
        SetResource,
        InvokeGlobalEvent,
        DebugMessage
    }
}