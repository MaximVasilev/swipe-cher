﻿using System;
using System.Collections;
using System.Collections.Generic;
using Gamebase.Miscellaneous;
using Gamebase.Systems.Debugging;
using Gamebase.Systems.GlobalEvents;
using Gamebase.Systems.Resources;
using Gamebase.Systems.Sound;
using Sirenix.OdinInspector;
using UnityEngine;

namespace Gamebase.Systems.GameLogic
{
    /// <summary>
    /// Отвечает за установление связей между игровыми событиями и пользовательскими действиями, установленными в настройках системы.
    /// С помощью этой системы можно собрать набор простых игровых правил, например выдачу ресурсов, звуковые оповещения о событиях, и т.д.
    /// Не рекомендуется злоупотреблять использованием данной системы и держать в ней простые связи событие-действие в небольшом количестве.
    /// Удобно для прототипирования.
    /// </summary>
    [TypeInfoBox("Отвечает за установление связей между игровыми событиями и пользовательскими действиями, установленными в настройках системы. " +
                 "С помощью этой системы можно собрать набор простых игровых правил, например выдачу ресурсов, звуковые оповещения о событиях, и т.д. " +
                 "Не рекомендуется злоупотреблять использованием данной системы и держать в ней простые связи событие-действие в небольшом количестве. " +
                 "Удобно для прототипирования.")]
    [AddComponentMenu("Gamebase/Game Logic System")]
    [DisallowMultipleComponent]
    public class GameLogicSystem : Singleton<GameLogicSystem>
    {
//         public static GameLogicSystem instance;
// 
//         private void Awake()
//         {
//             instance = this;
//         }

        [SerializeField] private GameLogicSettings settings;

//         private void Start()
//         {
//             Initialize();
//         }

        public override void Initialize()
        {
            if (settings == null)
            {
                DebugSystem.LogError("Not initialized GameLogicSettings!");
                return;
            }

            foreach (var binding in settings.eventBindings)
            {
                foreach (var bindingAction in binding.actions)
                {
                    Action action = GetAction(bindingAction);
                    if (action != null)
                        GlobalEvents.GlobalEventsSystem.Instance.Subscribe(binding.globalEventType, action);
                }
            }
        }

        private Action GetAction(GlobalEventBindingAction bindingAction)
        {
            switch (bindingAction.type)
            {
                case GlobalEventBindingType.PlaySound:
                    return () => SoundSystem.Instance.PlaySound(bindingAction.sound);
                case GlobalEventBindingType.PlayMusic:
                    return () => SoundSystem.Instance.PlayMusic(bindingAction.music);
                case GlobalEventBindingType.AddResource:
                    return () => ResourcesSystem.Instance.AddResource(bindingAction.resourceValue);
                case GlobalEventBindingType.SetResource:
                    return () => ResourcesSystem.Instance.SetResource(bindingAction.resourceValue);
                case GlobalEventBindingType.InvokeGlobalEvent:
                    return () => GlobalEventsSystem.Instance.Invoke(bindingAction.invokeEventType);
                case GlobalEventBindingType.DebugMessage:
                    return () => DebugSystem.Log(bindingAction.debugMessage);
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }
    }
}