﻿
using System;
using UnityEngine;

namespace Gamebase.Systems.Resources
{
    [Serializable]
    public abstract class Resource
    {
        public abstract void GetSavedValue();
        public abstract int GetCurrentValue();

        public abstract void Add(int count);
        public abstract void Set(int count);

        public void Clear()
        {
            Set(0);
        }
    }
}