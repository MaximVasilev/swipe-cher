﻿using Gamebase.Systems.Resources;
using UnityEngine;

namespace Utilities
{
    public abstract class PlayerPrefsResource : Resource
    {
        protected abstract string PlayerPrefsKey { get; }

        private int _currentValue;

        private int CurrentValue
        {
            get { return _currentValue; }
            set
            {
                if (value < 0) return;

                _currentValue = value;
                PlayerPrefs.SetInt(PlayerPrefsKey, _currentValue);
            }
        }

        public override void GetSavedValue()
        {
            CurrentValue = PlayerPrefs.GetInt(PlayerPrefsKey, 0);
        }

        public override int GetCurrentValue()
        {
            return CurrentValue;
        }

        public override void Add(int count)
        {
            if (count < 0) return;

            CurrentValue += count;
        }

        public override void Set(int count)
        {
            CurrentValue = Mathf.Max(count, 0);
        }
    }
}