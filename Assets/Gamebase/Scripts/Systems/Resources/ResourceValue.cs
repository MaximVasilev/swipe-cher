﻿using System;
using TypeReferences;

namespace Gamebase.Systems.Resources
{
    /// <summary>
    /// Класс, содержащий тип игрового ресурса (из ResourceSystem) и значение
    /// </summary>
    [Serializable]
    public class ResourceValue
    {
        [ClassExtends(typeof(Resource),Grouping = ClassGrouping.None)] 
        public ClassTypeReference resourceType = typeof(Resource);
        public int value;
    }
}