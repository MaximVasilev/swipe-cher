﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Gamebase.Miscellaneous;
using Gamebase.Systems.Debugging;
using Gamebase.Systems.GameLogic;
using Sirenix.OdinInspector;
using TypeReferences;
using UnityEngine;

namespace Gamebase.Systems.Resources
{
    /// <summary>
    /// Система ресурсов предоставляет интерфейс для работы с игровыми ресурсами целочисленного значения (опыт, валюта, очки на уровне, кол-во бонусов и т.д.). Ресурсы могут быть как постоянными (сохраняющими свое значение между сессиями), так и временными (например только в рамках одного уровня). Для создания нового вида ресурса необходимо создать класс наследник класса Resource и имплементировать все необходимые методы. Система ресурсов автоматически находит в проекте все классы, наследуемые от Resource и регистрирует их для использования.
    /// </summary>
    [TypeInfoBox("Система ресурсов предоставляет интерфейс для работы с игровыми ресурсами целочисленного значения (опыт, валюта, очки на уровне, кол-во бонусов и т.д.). Ресурсы могут быть как постоянными (сохраняющими свое значение между сессиями), так и временными (например только в рамках одного уровня). Для создания нового вида ресурса необходимо создать класс наследник класса Resource и имплементировать все необходимые методы. Система ресурсов автоматически находит в проекте все классы, наследуемые от Resource и регистрирует их для использования.")]
    [AddComponentMenu("Gamebase/Resources System")]
    [DisallowMultipleComponent]
    public class ResourcesSystem : Singleton<ResourcesSystem>
    {
        //public static ResourcesSystem Instance;

        /*private void Awake()
        {
            Instance = this;
        }*/

        //[SerializeField]
        //private bool AutoInitializeOnStart = true;

        
       /* private void Start()
        {
            if (AutoInitializeOnStart)
                Initialize();
        }*/

        List<Resource> resources;

        Action<Resource> OnResourceCountChanged;

        public override void Initialize()
        {
            List<Type> resourcesTypes = AppDomain.CurrentDomain.GetAssemblies().SelectMany(a => a.GetTypes())
                .Where(t => t.IsSubclassOf(typeof(Resource)) && !t.IsAbstract).ToList();

            resources = new List<Resource>();
            for (int i = 0; i < resourcesTypes.Count; i++)
            {
                Resource res = (Resource)Activator.CreateInstance(resourcesTypes[i], null);
                resources.Add(res);
            }

            foreach (var resource in resources)
            {
                resource.GetSavedValue();
            }
        }


        public void AddResource<T>(int count) where T : Resource
        {
            var resource = resources.FirstOrDefault(t => t is T);
            AddResource(resource, count);
        }

        private void AddResource(Resource resource, int count)
        {
            if (resource != null)
            {
                DebugSystem.Log($"ResourcesSystem - AddResource ({resource} {count})");
                resource.Add(count);
            }
            else
            {
                var exceptionMessage = $"ResourcesSystem - There's no such resource! Type = {resource}";
                DebugSystem.LogError(exceptionMessage);
                throw new Exception(exceptionMessage);
            }
        }

        public void AddResource(ResourceValue resourceValue)
        {
            var resource = resources.FirstOrDefault(t => t.GetType() == resourceValue.resourceType.Type);
            AddResource(resource, resourceValue.value);
        }

        public void SetResource<T>(int count) where T : Resource
        {
            var resource = resources.FirstOrDefault(t => t is T);
            if (resource != null)
            {
                DebugSystem.Log($"ResourcesSystem - SetResource ({typeof(T)} {count})");
                resource.Set(count);
            }
            else
            {
                var exceptionMessage = $"ResourcesSystem - There's no such resource! Type = {typeof(T)}";
                DebugSystem.LogError(exceptionMessage);
                throw new Exception(exceptionMessage);
            }
        }

        private void SetResource(Resource resource, int count)
        {
            if (resource != null)
            {
                DebugSystem.Log($"ResourcesSystem - AddResource ({resource} {count})");
                resource.Set(count);
            }
            else
            {
                var exceptionMessage = $"ResourcesSystem - There's no such resource! Type = {resource}";
                DebugSystem.LogError(exceptionMessage);
                throw new Exception(exceptionMessage);
            }
        }

        public void SetResource(ResourceValue resourceValue)
        {
            var resource = resources.FirstOrDefault(t => t.GetType() == resourceValue.resourceType.Type);
            SetResource(resource, resourceValue.value);
        }

        public int GetResourceCount<T>() where T : Resource
        {
            var resource = resources.FirstOrDefault(t => t is T);
            if (resource != null)
                return resource.GetCurrentValue();
            else
            {
                var exceptionMessage = $"ResourcesSystem - There's no such resource! Type = {typeof(T)}";
                DebugSystem.LogError(exceptionMessage);
                throw new Exception(exceptionMessage);
            }
        }

        private void OnDestroy()
        {
            OnResourceCountChanged = null;
        }
    }
}