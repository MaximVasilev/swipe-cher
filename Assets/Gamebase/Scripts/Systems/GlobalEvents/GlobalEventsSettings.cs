﻿using System;
using System.Linq;
using Gamebase.Miscellaneous;
using Gamebase.Systems;
using Sirenix.OdinInspector;
using UnityEngine;

[CreateAssetMenu(menuName = "Gamebase/GlobalEventsSettings Settings")]
public class GlobalEventsSettings : ScriptableObject
{
    [Tooltip("Список глобальных событий"), OnValueChanged("RebuildEnumCode", includeChildren: true)]
    [SerializeField]
    private GlobalEventElement[] events;

#if UNITY_EDITOR
    
    [ContextMenu("Manual Rebuild Enum Code")]
    /// <summary>
    /// Пересборка файла с перечислением
    /// </summary>
    public void RebuildEnumCode()
    {
        DynamicEnums.ReplaceAndSave("GlobalEventTypeEnum",events.Select(e=>e.name).ToList());
    }

#endif
}



[Serializable]
public class GlobalEventElement
{
    [Delayed, Tooltip("Название события (на английском, без пробелов и специальных символов)")]
    public string name = "";
}


