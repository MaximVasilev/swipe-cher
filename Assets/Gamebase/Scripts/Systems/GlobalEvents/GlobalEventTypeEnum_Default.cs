#if !(GAMEBASE_INITIALIZED)
namespace Gamebase.Systems.GlobalEvents
{
    public enum GlobalEventType
    {
        None,
        LevelInitialized,
        LevelVictory,
        LevelDefeat,
        ProgressLevelChanged,
        XPChanged
    };
}
#endif