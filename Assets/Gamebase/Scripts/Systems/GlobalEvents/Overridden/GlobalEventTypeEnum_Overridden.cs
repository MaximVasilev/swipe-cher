#if (GAMEBASE_INITIALIZED)
namespace Gamebase.Systems.GlobalEvents
{
        public enum GlobalEventType
        {
	    None,
            LevelInitialized,
            LevelVictory,
            LevelDefeat,
            ProgressLevelChanged,
            XPChanged,
LevelRestart=1165497093,

        };
}
#endif