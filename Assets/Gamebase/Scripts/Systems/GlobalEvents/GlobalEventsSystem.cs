using System;
using System.Collections.Generic;
using Gamebase.Miscellaneous;
using Gamebase.Systems.Debugging;
using Sirenix.OdinInspector;
using UnityEngine;

namespace Gamebase.Systems.GlobalEvents
{
    /// <summary>
    /// Система глобальных игровых событий. Дает функционал для подписывания и вызова событий. Для добавления новых типов событий необходимо править перечисление GlobalEventType. Использовать разумно, не все события в игре стоит обрабатывать через систему глобальных событий. Удобно для прототипирования.
    /// </summary>
    [TypeInfoBox("Система глобальных игровых событий. Дает функционал для подписывания и вызова событий. Для добавления новых типов событий необходимо править перечисление GlobalEventType. Использовать разумно, не все события в игре стоит обрабатывать через систему глобальных событий. Удобно для прототипирования.")]
    [AddComponentMenu("Gamebase/Global Events System")]
    [DisallowMultipleComponent]
    public class GlobalEventsSystem : Singleton<GlobalEventsSystem>
    {
        Dictionary<GlobalEventType, Action> events;

        public override void Initialize()
        {
            events = new Dictionary<GlobalEventType, Action>();
            var values = Enum.GetValues(typeof(GlobalEventType));
            for (int i = 0; i < values.Length; i++)
            {
                events.Add((GlobalEventType)values.GetValue(i), (() => {}));
            }
        }

        public void ClearAllEvents()
        {
            var values = Enum.GetValues(typeof(GlobalEventType));
            for (int i = 0; i < Enum.GetValues(typeof(GlobalEventType)).Length; i++)
            {
                events[(GlobalEventType)values.GetValue(i)] = null;
            }
        }

        public void Subscribe(GlobalEventType type, Action action)
        {
            DebugSystem.Log($"GlobalEventsSystem - Subscribe ({type})");
            events[type] += action;
        }

        public void Unsubscribe(GlobalEventType type, Action action)
        {
            DebugSystem.Log($"GlobalEventsSystem - Unsubscribe ({type})");
            events[type] -= action;
        }

        public void Invoke(GlobalEventType type)
        {
            DebugSystem.Log($"GlobalEventsSystem - Invoke Global Event ({type})");
            events[type]?.Invoke();
        }

        private void OnDestroy()
        {
            ClearAllEvents();
        }
    }
}