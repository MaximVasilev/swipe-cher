using System.Collections;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine;

namespace Gamebase.Systems.Settings
{
    /// <summary>
    /// Провайдер настроек позволяет собрать в одном месте конфигурации необходимые другим классам. Использовать с осторожностью, удобен для прототипирования.
    /// </summary>
    [TypeInfoBox("Провайдер настроек позволяет собрать в одном месте конфигурации (ScriptableObject) необходимые другим классам. Использовать с осторожностью, удобен для прототипирования.")]
    [DisallowMultipleComponent]
    public abstract class SettingsProvider : MonoBehaviour
    {
        public static SettingsProvider instance;

        private void Awake()
        {
            if (instance == null)
            {
                instance = this;
            }
            else
            {
                DontDestroyOnLoad(gameObject);
            }
        }
    }
}
