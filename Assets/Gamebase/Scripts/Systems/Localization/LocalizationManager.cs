﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using Gamebase.Miscellaneous;
using Gamebase.Systems.Debugging;
using Sirenix.OdinInspector;
using UnityEngine;

namespace Gamebase.Systems.Localization
{
    /// <summary>
    /// Менеджер локализации позволяет локализовать игровые тексты и изображения. На все компоненты текста (поддерживается TextMeshPro), которым необходима локализация нужно добавлять компонент LocalizedText и указывать ключ локализации из конфига (LocalizationData). Файлы локализации по умолчанию должны быть расположены в папке Resources/Localization. Названия подпапок для разных языков должны соответствовать перечислению UnityEngine.SystemLanguage. Внутри подпапок может содержаться любое количество JSON локализационных файлов
    /// </summary>
    [TypeInfoBox("Менеджер локализации позволяет локализовать игровые тексты и изображения. На все компоненты текста (поддерживается TextMeshPro), которым необходима локализация нужно добавлять компонент LocalizedText и указывать ключ локализации из конфига (LocalizationData). Файлы локализации по умолчанию должны быть расположены в папке Resources/Localization. Названия подпапок для разных языков должны соответствовать перечислению UnityEngine.SystemLanguage. Внутри подпапок может содержаться любое количество JSON локализационных файлов")]
    [DisallowMultipleComponent]
    public class LocalizationManager : Singleton<LocalizationManager>
    {
        [SerializeField] private bool overrideLocalization;
        [ShowIf("overrideLocalization")]
        [SerializeField] private SystemLanguage overrideLocIndex;
        [SerializeField] private SystemLanguage defaultLocIndex = SystemLanguage.English;

        //public static LocalizationManager instance;

        private Dictionary<string, string> localizedText;
        private string missingTextString = "%NOTFOUND%";

        private SystemLanguage currentLang;


        public override void Initialize()
        {
            LoadLocalizedText();
        }

        public void LoadLocalizedText()
        {
            SystemLanguage lang = (SystemLanguage)GetCurrentLanguage();

            ChangeLanguage(lang);
        }

        bool HasLocalizationFileForLanguage(SystemLanguage lang)
        {
            var path = $"Localization/{lang}";
            TextAsset[] allConfigs = UnityEngine.Resources.LoadAll<TextAsset>(path);
            return (allConfigs.Length > 0);
        }

        public void ChangeLanguage(SystemLanguage lang)
        {
            localizedText = new Dictionary<string, string>();

            currentLang = lang;
            PlayerPrefs.SetInt("ChoosenLanguageIndex", (int)lang);

            var path = $"Localization/{lang}";
            TextAsset[] allConfigs = UnityEngine.Resources.LoadAll<TextAsset>(path);

            for (int i = 0; i < allConfigs.Length; i++)
            {
                string jsonText = allConfigs[i].text;
                LocalizationData loadedData = JsonUtility.FromJson<LocalizationData>(jsonText);

                for (int j = 0; j < loadedData.items.Length; j++)
                {
                    localizedText.Add(loadedData.items[j].key, loadedData.items[j].value);
                }
            }
        }

        public string GetLocalizedValue(string key)
        {
            string result = missingTextString;
            if (localizedText.ContainsKey(key))
            {
                result = localizedText[key];
            }

            return result;
        }
        /// <summary>
        /// Возвращает индекс системного языка (соответствует перечислению UnityEngine.SystemLanguage)
        /// </summary>
        /// <returns></returns>
        public SystemLanguage GetFirstLanguage()
        {
            SystemLanguage lang = overrideLocalization ? overrideLocIndex : Application.systemLanguage;
            return lang;
        }

        public SystemLanguage GetCurrentLanguage()
        {
            if (overrideLocalization)
                return GetFirstLanguage();

            if (PlayerPrefs.HasKey("ChoosenLanguageIndex"))
            {
                return (SystemLanguage)PlayerPrefs.GetInt("ChoosenLanguageIndex");
            }
            else
            {
                var currentLocalizationIndex = GetFirstLanguage();
                PlayerPrefs.SetInt("ChoosenLanguageIndex", (int)currentLocalizationIndex);
                return currentLocalizationIndex;
            }
        }

        public bool CurrentLanguageIs(SystemLanguage lang)
        {
            return currentLang == lang;
        }
    }
}