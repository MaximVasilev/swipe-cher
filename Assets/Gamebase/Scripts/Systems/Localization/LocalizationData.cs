﻿using UnityEngine;

namespace Gamebase.Systems.Localization
{
    [System.Serializable]
    public class LocalizationData
    {
        public LocalizationItem[] items;
    }

    [System.Serializable]
    public class LocalizationItem
    {
        public string key;
        [TextArea(minLines:1,maxLines:10)]
        public string value;
    }
}