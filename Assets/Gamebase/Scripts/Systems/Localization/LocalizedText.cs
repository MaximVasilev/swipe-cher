﻿using Gamebase.Systems.Debugging;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Gamebase.Systems.Localization
{
    [DisallowMultipleComponent]
    [RequireComponent(typeof(TextMeshProUGUI))]
    public class LocalizedText : MonoBehaviour
    {
        [SerializeField] string key;

        [HideInInspector]
        public TextMeshProUGUI text;
        private void Awake()
        {
            ResetValue();
        }

        public string GetValue()
        {
            if (string.IsNullOrEmpty(key))
            {
                DebugSystem.LogWarning($"Localization Manager - {name} - Нет локализационного ключа. Оставляем значение по умолчанию");
                return text.text;
            }

            if (LocalizationManager.Instance != null)
            {
                return LocalizationManager.Instance.GetLocalizedValue(key);
            }
            return text.text;
        }

        public void ResetValue()
        {
            if (ReferenceEquals(text, null))
                text = GetComponent<TextMeshProUGUI>();
            text.text = GetValue();
        }

        public void SetKey(string newKey)
        {
            key = newKey;
            ResetValue();
        }
    }
}