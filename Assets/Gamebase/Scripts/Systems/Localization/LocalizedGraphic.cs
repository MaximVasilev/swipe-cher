﻿using System;
using System.Collections.Generic;
using System.Linq;
using Gamebase.Systems.Debugging;
using UnityEngine;
using UnityEngine.UI;

namespace Gamebase.Systems.Localization
{
    /// <summary>
    /// Компонент для локализации графики компонента Image, автоматически берет соотетствующую текущему индексу языка графику из поля graphics
    /// </summary>
    [RequireComponent(typeof(Image))]
    public class LocalizedGraphic : MonoBehaviour
    {
        [Serializable]
        public class LocalizedGraphicSetting
        {
            [Tooltip("Язык, для которого будет использоваться спрайт")]
            public SystemLanguage language;
            [Tooltip("Ссылка на файл спрайта")]
            public Sprite sprite;
        }

        [Tooltip("Спрайты для разных языков")]
        [SerializeField] List<LocalizedGraphicSetting> settings;

        private Image thisImage;

        private void Awake()
        {
            ResetValue();
        }

        Sprite GetSprite()
        {
            if (LocalizationManager.Instance != null)
            {
                var sprite = settings.FirstOrDefault(e => e.language == LocalizationManager.Instance.GetCurrentLanguage())?.sprite;
                if (sprite == null)
                {
                    DebugSystem.LogWarning($"LocalizedGraphic - У объекта {name} нет спрайта для текущего языка {LocalizationManager.Instance.GetCurrentLanguage()}! Будет использован спрайт для языка по умолчанию!");

                    sprite = settings.FirstOrDefault(e => e.language == LocalizationManager.Instance.GetFirstLanguage())
                        ?.sprite;

                    if (sprite == null)
                        DebugSystem.LogError($"LocalizedGraphic - У объекта {name} нет спрайта для языка по умолчанию {LocalizationManager.Instance.GetFirstLanguage()}!");

                    return sprite;
                }
            }
            DebugSystem.LogError($"LocalizedGraphic - Объект {name} не обнаружил экземпляр системы локализации, локализация невозможна!");
            return null;
        }

        public void ResetValue()
        {
            if (ReferenceEquals(thisImage, null))
                thisImage = GetComponent<Image>();
            thisImage.sprite = GetSprite();
        }
    }
}
