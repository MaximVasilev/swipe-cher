﻿using System.Collections;
using System.Collections.Generic;
using Gamebase.Systems.Resources;
using UnityEngine;

namespace Gamebase.Systems.Progress
{
    public class ProgressLevel : Resource
    {
        private const string playerPrefsKey = "Resource_ProgressLevel";

        private int _currentValue;

        private int CurrentValue
        {
            get { return _currentValue; }
            set
            {
                if (value < 0) return;

                _currentValue = value;
                PlayerPrefs.SetInt(playerPrefsKey, _currentValue);
            }
        }

        public override void GetSavedValue()
        {
            CurrentValue = PlayerPrefs.GetInt(playerPrefsKey, 0);
        }

        public override int GetCurrentValue()
        {
            return CurrentValue;
        }

        public override void Add(int count)
        {
            if (count < 0) return;

            CurrentValue += count;
        }

        public override void Set(int count)
        {
            CurrentValue = count;
        }
    }
}