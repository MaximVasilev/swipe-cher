﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Gamebase.Miscellaneous;
using Gamebase.Systems.Debugging;
using Sirenix.OdinInspector;
using UnityEngine;

namespace Gamebase.Systems.Progress
{
    [CreateAssetMenu(menuName = "Gamebase/Progress Settings")]
    public class ProgressSettings : ScriptableObject
    {
        public bool UseXP;
        [ShowIf("UseXP")]
        public bool OpenLevelsWithXP;
        [ShowIf("UseXP")]
        public bool ClearXPAfterEachLevel;
        [ShowIf("UseXP")]
        public List<ProgressLevelSetting> levelSettings;
        [OnValueChanged("RebuildEnumCode", includeChildren: true)]
        public List<GameFeatureSetting> featureSettings;

#if UNITY_EDITOR
        [ContextMenu("Manual Rebuild Enum Code")]
        /// <summary>
        /// Пересборка файла с перечислением
        /// </summary>
        public void RebuildEnumCode()
        {
            if (featureSettings != null)
                DynamicEnums.ReplaceAndSave("GameFeatureType", featureSettings.Select(e => e.Name).ToList());
        }
#endif

        public GameFeatureSetting GetFeatureSetting(GameFeatureType type)
        {
            for (int i = 0; i < featureSettings.Count; i++)
            {
                if (featureSettings[i].Name.GetHashCode() == type.GetHashCode())
                    return featureSettings[i];
            }

            DebugSystem.LogError($"ProgressSettings - Не найдена фича типа {type}!");
            return null;
        }
    }

    [Serializable]
    public class GameFeatureSetting
    {
        [Delayed]
        public string Name;
        [TextArea]
        public string Description;
        public int ProgressLevelRequired;
    }

    [Serializable]
    public class ProgressLevelSetting
    {
        public int XpRequired;
    }
}