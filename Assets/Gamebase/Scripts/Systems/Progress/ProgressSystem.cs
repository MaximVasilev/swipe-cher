using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Gamebase.Miscellaneous;
using Gamebase.Systems.GlobalEvents;
using Gamebase.Systems.Resources;
using Sirenix.OdinInspector;
using UnityEngine;

namespace Gamebase.Systems.Progress
{
    /// <summary>
    /// Система прогресса предоставляет интерфейс для работы с уровнем игрового аватара и опытом, а также с разблокируемыми фичами игры, которые должны открываться только по достижении игровым аватаром определенного уровня.
    /// </summary>
    [TypeInfoBox("Система прогресса предоставляет интерфейс для работы с уровнем игрового аватара и опытом, а также с разблокируемыми фичами игры, которые должны открываться только по достижении игровым аватаром определенного уровня.")]
    [AddComponentMenu("Gamebase/Progress System")]
    [DisallowMultipleComponent]
    public class ProgressSystem : Singleton<ProgressSystem>
    {
//         public static ProgressSystem instance;
// 
//         private void Awake()
//         {
//             instance = this;
//         }

        public override void Initialize()
        {
            
        }

        [SerializeField] ProgressSettings settings;

        private Action<int> OnProgressLevelChanged;
        public int CurrentProgressLevel
        {
            get => ResourcesSystem.Instance.GetResourceCount<ProgressLevel>();
            private set
            {
                var oldVal = CurrentProgressLevel;
                if (oldVal != value)
                {
                    ResourcesSystem.Instance.SetResource<ProgressLevel>(value);
                    OnProgressLevelChanged?.Invoke(CurrentProgressLevel);
                    GlobalEventsSystem.Instance.Invoke(GlobalEventType.ProgressLevelChanged);
                }
            }
        }

        private Action<int> OnXPChanged;
        public int CurrentXP
        {
            get
            {
                NoUseXPErrorCheck();
                return ResourcesSystem.Instance.GetResourceCount<XP>();
            }
            private set
            {
                NoUseXPErrorCheck();
                var oldVal = CurrentXP;
                if (oldVal != value)
                {
                    ResourcesSystem.Instance.SetResource<XP>(value);
                    OnXPChanged?.Invoke(CurrentXP);
                    GlobalEventsSystem.Instance.Invoke(GlobalEventType.XPChanged);
                    if (settings.OpenLevelsWithXP)
                        CheckForNewLevel();
                }
            }
        }

        private void NoUseXPErrorCheck()
        {
            if (!settings.UseXP)
                Debug.LogError(
                    "[ProgressSystem] - В настройках системы прогресса отключено использование XP, но произошел запрос на использование его или связанных с ним настроек! Включите использование XP, либо не используйте его в коде!");
        }

        ProgressLevelSetting NextLvlSetting
        {
            get
            {
                NoUseXPErrorCheck();
                return settings.levelSettings.Count > CurrentProgressLevel
                    ? settings.levelSettings[CurrentProgressLevel]
                    : null;
            }
        }

        public void AddXP(int count)
        {
            CurrentXP += count;
        }

        public void OpenNextLevel()
        {
            CurrentProgressLevel++;
        }

        private void CheckForNewLevel()
        {
            NoUseXPErrorCheck();
            while (NextLvlSetting != null && CurrentXP >= NextLvlSetting.XpRequired)
            {
                if (settings.ClearXPAfterEachLevel)
                    CurrentXP -= NextLvlSetting.XpRequired;
                OpenNextLevel();
            }
        }

        public bool FeatureIsUnlocked(GameFeatureType type)
        {
            var featureSetting = settings.GetFeatureSetting(type);
            if (featureSetting == null)
                throw new Exception($"Не найдено фичи типа {type} в настройках {settings.name}!");


            return CurrentProgressLevel >= featureSetting.ProgressLevelRequired;
        }
    }
}