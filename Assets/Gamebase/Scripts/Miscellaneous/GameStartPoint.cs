﻿using System;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Gamebase.Miscellaneous
{
    [TypeInfoBox("Точка входа в игру. Должно быть на самой первой сцене, где инициализируются все системы")]
    class GameStartPoint : MonoBehaviour
    {
        public static bool IsInitialized { get; private set; }

        [SerializeField] private SceneField LoadingScene;
        [SerializeField] private SceneField FirstScene;

        [Tooltip("Нужно ли загружать сцену загрузки")]
        [SerializeField]
        private bool needLoadingScreen = false;
        [Tooltip("Нужно ли сразу загружать следующую сцену")]
        [SerializeField] private bool autoStartLoading = true;

        public void Awake()
        {
            if (!IsInitialized)
            {
                IsInitialized = true;

                if (PlayModeController.IsForwardingToInitialization)
                {
                    GoToSceneName = PlayModeController.GetBackToSceneName;
                    PlayModeController.FinalizeForwarding();
                }
            }
        }

        private string GoToSceneName;
        private void Start()
        {
            if (!string.IsNullOrEmpty(GoToSceneName))
            {
                SceneManager.LoadSceneAsync(GoToSceneName);
                GoToSceneName = null;
                return;
            }

            if (autoStartLoading)
                SceneManager.LoadSceneAsync(!needLoadingScreen ? FirstScene : LoadingScene);
        }
    }

}