﻿using System;
using System.IO;
using UnityEngine;

namespace Gamebase.Miscellaneous
{
    [Serializable]
    public abstract class JSONSerialized<T> where T : new()
    {
        protected JSONSerialized()
        {
            SaveToJSON();
        }

        public void SaveToJSON()
        {
            File.WriteAllText(Application.persistentDataPath + $"/{typeof(T)}.json", JsonUtility.ToJson(this, true));
        }

        public static T LoadOrCreate()
        {
            //Ищем файл прогресса
            string path = Application.persistentDataPath + $"/{typeof(T)}.json";
            string jsonString = null;

            if (File.Exists(path)) //Если он есть - читаем
                jsonString = File.ReadAllText(path);

            if (jsonString != null)
            {
                T config = JsonUtility.FromJson<T>(jsonString);
                return config;
            }
            else
            {
                T newConfig = new T();
                return newConfig;
            }
        }
    }
}