﻿using UnityEngine;

namespace Gamebase.Miscellaneous
{
    public class StaticScriptableObject<T> : ScriptableObject where T : ScriptableObject
    {
        private static T _instance;
        public static T Instance
        {
            get
            {
                if (_instance == null)
                {
                    var path = $"Settings/{typeof(T).Name}";
                    _instance = Resources.Load(path) as T;
                }

                return _instance;
            }
        }
    }
}