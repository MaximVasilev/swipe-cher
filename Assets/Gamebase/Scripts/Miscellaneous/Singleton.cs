﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Gamebase.Miscellaneous
{

    public abstract class Singleton<T> : MonoBehaviour where T : Component
    {
        private static bool _shuttingDown = false;
        private static T _instance;

        [SerializeField]
        private bool AutoInitializeOnStart = true;

        [SerializeField]
        private bool DontDestroyOnLoad = true;

        public abstract void Initialize();

        protected virtual void Start()
        {
            if (AutoInitializeOnStart)
                Initialize();
        }

        protected virtual void Awake()
        {
            if (_instance == null)
            {
                if (this is T)
                {
                    _instance = this as T;
                    if (DontDestroyOnLoad && Application.isPlaying)
                        DontDestroyOnLoad(gameObject);
                }
            }
            else if (Application.isPlaying)
            {
                Debug.LogWarning($"[Singleton] Instance {typeof(T)} already exists. Destroying {name}...");
                DestroyImmediate(gameObject);
            }

        }

        public static T Instance => _instance;

        public static bool Exists => _instance != null;
    }
}