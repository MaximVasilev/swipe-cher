﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
#if UNITY_EDITOR
using UnityEditor;
#endif
using UnityEngine;

namespace Gamebase.Miscellaneous
{
#if UNITY_EDITOR
    public class DynamicEnums
    {
        static string GetTemplateAndOverridenFiles(string className, out string overridenFile)
        {
            var files = Directory
                .EnumerateFiles(Application.dataPath, $"{className}_Default.cs", SearchOption.AllDirectories).ToList();
            if (files.Count == 0)
            {
                Debug.LogError(
                    $"[DynamicEnums] Can`t find file |{className}_Default.cs| in dirs under |{Application.dataPath}|");
                overridenFile = null;
                return null;
            }

            var templateFile = File.ReadAllText($"{Path.GetDirectoryName(files[0])}/{className}_Template.txt");
            overridenFile = $"{Path.GetDirectoryName(files[0])}/Overridden/{className}_Overridden.cs";

            return templateFile;
        }

        public static void ReplaceAndSave(string className, List<string> valuesNames)
        {
            var templateFile = GetTemplateAndOverridenFiles(className, out var overridenFile);
            if (templateFile == null) return;

            var values = valuesNames.Where(e => !string.IsNullOrEmpty(e)).ToList();

            if (values.Count > 0)
            {
                foreach (var val in values)
                    templateFile = templateFile.Replace("@1", $"{val}={val.GetHashCode()},\n@1");

                templateFile = templateFile.Replace("@1", "").Replace(",\n\n}", "\n}");


                if (!Directory.Exists(Path.GetDirectoryName(overridenFile)))
                    Directory.CreateDirectory(Path.GetDirectoryName(overridenFile));
                File.WriteAllText(overridenFile, templateFile);



                AssetDatabase.Refresh();

                Debug.Log($"[DynamicEnums] Enum({className}) was written to file |{Path.GetFullPath(overridenFile)}|");
            }
        }

        public static void Init(string className)
        {
            var templateFile = GetTemplateAndOverridenFiles(className, out var overridenFile);
            if (templateFile == null) return;

            templateFile = templateFile.Replace("@1", "").Replace(",\n\n}", "\n}");

            if (!Directory.Exists(Path.GetDirectoryName(overridenFile)))
                Directory.CreateDirectory(Path.GetDirectoryName(overridenFile));
            File.WriteAllText(overridenFile, templateFile);

            AssetDatabase.Refresh();

            Debug.Log($"[DynamicEnums] Init({className}) - |{Path.GetFullPath(overridenFile)}|");
        }

        public static void Reset(string className)
        {
            var templateFile = GetTemplateAndOverridenFiles(className, out var overridenFile);
            if (templateFile == null) return;

            if (File.Exists(overridenFile))
                File.Delete(overridenFile);

            AssetDatabase.Refresh();

            Debug.Log($"[DynamicEnums] Reset({className}) - |{Path.GetFullPath(overridenFile)}|");
        }
    }
#endif
}