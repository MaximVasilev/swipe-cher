﻿using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Gamebase.Miscellaneous
{
    [TypeInfoBox("При запуске игры с этой сцены - автоматически перейдет на первую сцену (для возможности запускать игру с любой сцены)")]
    class PlayModeController : MonoBehaviour
    {
        [SerializeField] private SceneField InitializationScene;
        [SerializeField] private bool GetBackHere;

        public static bool IsForwardingToInitialization { get; private set; }
        public static string GetBackToSceneName;
        void Awake()
        {
            if (!GameStartPoint.IsInitialized)
            {
                IsForwardingToInitialization = true;

                if (GetBackHere)
                    GetBackToSceneName = SceneManager.GetActiveScene().name;

                //Отключаем все объекты на сцене, чтобы в них не вызвался Awake
                var allobjects = FindObjectsOfType<GameObject>();
                foreach (var component in allobjects)
                {
                    component.SetActive(false);
                }

                SceneManager.LoadScene(InitializationScene);
            }
        }

        public static void FinalizeForwarding()
        {
            IsForwardingToInitialization = false;
            GetBackToSceneName = null;
        }
    }
}