﻿using Gamebase.Miscellaneous;

namespace Gamebase.Editor
{
    public class SettingsProviderProjectInitializer : IChildProjectInitializer
    {
        public void OnChildProjectInit()
        {
            CodeGenerator.GenerateFromTemplate("SettingsProvider");
        }

        public void OnChildProjectReset()
        {
            CodeGenerator.RemoveGeneratedClass("SettingsProvider");
        }
    }
}