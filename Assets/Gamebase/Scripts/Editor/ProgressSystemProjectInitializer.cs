﻿using Gamebase.Miscellaneous;
using Gamebase.Systems.Progress;

namespace Gamebase.Editor
{
    public class ProgressSystemProjectInitializer : IChildProjectInitializer
    {
        public void OnChildProjectInit()
        {
            DynamicEnums.Init("GameFeatureType");
            CodeGenerator.CreateSettingsInstance<ProgressSettings>("Progress Settings");
        }

        public void OnChildProjectReset()
        {
            DynamicEnums.Reset("GameFeatureType");
            CodeGenerator.RemoveSettingsInstance("Progress Settings");
        }
    }
}
