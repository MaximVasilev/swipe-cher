﻿using Gamebase.Miscellaneous;
using Gamebase.Systems.GameLogic;
using UnityEngine;

namespace Gamebase.Editor
{
    public class GameLogicSystemProjectInitializer : IChildProjectInitializer
    {
        public void OnChildProjectInit()
        {
            CodeGenerator.CreateSettingsInstance<GameLogicSettings>("Game Logic Settings");
        }

        public void OnChildProjectReset()
        {
            CodeGenerator.RemoveSettingsInstance("Game Logic Settings");
        }
    }
}