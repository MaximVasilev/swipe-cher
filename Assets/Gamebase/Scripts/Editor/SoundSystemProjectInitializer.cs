﻿using Gamebase.Miscellaneous;
using Gamebase.Systems.Sound;

namespace Gamebase.Editor
{
    public class SoundSystemProjectInitializer : IChildProjectInitializer
    {
        public void OnChildProjectInit()
        {
            DynamicEnums.Init("SoundEnum");
            DynamicEnums.Init("MusicEnum");
            CodeGenerator.CreateSettingsInstance<SoundSettings>("Sound Settings");
        }

        public void OnChildProjectReset()
        {
            DynamicEnums.Reset("SoundEnum");
            DynamicEnums.Reset("MusicEnum");
            CodeGenerator.RemoveSettingsInstance("Sound Settings");
        }
    }
}
