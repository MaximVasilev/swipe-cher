using System;
using Sirenix.OdinInspector;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;


namespace Gamebase.Editor
{
    /// <summary>
    /// Компонент, отвечающий за начальную инициализацию базы на дочеренм проекте. Пересоздает файлы локальных настроек и перечислений
    /// </summary>
    public static class ChildProjectInitializer
    {
#if !GAMEBASE_INITIALIZED
        [MenuItem("Gamebase/Initialize Gamebase")]
#endif
        static void InitializeGamebase()
        {
            InvokeOnAllChildInitializers((e) => e.OnChildProjectInit());

            //Заносим в дефайны флаг, что проинициализировали базу. После этого дефолтные перечисления будут исключены из сборки
            //TODO. Тут надо и другие конфигурации добавить, чтобы не только в STANDALONE было
            var defines = PlayerSettings.GetScriptingDefineSymbolsForGroup(BuildTargetGroup.Standalone);
            var splitDefines =
                new List<string>(defines.Split(new char[] { ';' }, System.StringSplitOptions.RemoveEmptyEntries));
            if (!splitDefines.Contains("GAMEBASE_INITIALIZED"))
                splitDefines.Add("GAMEBASE_INITIALIZED");
            defines = string.Join(";", splitDefines.ToArray());
            PlayerSettings.SetScriptingDefineSymbolsForGroup(BuildTargetGroup.Standalone, defines);
            Debug.Log($"[GAMEBASE_INITIALIZER] Gamebase init ok");
        }
#if GAMEBASE_INITIALIZED
        [MenuItem("Gamebase/Reset Gamebase")]
#endif
        static void ResetGamebase()
        {
            InvokeOnAllChildInitializers((e) => e.OnChildProjectReset());

            var defines = PlayerSettings.GetScriptingDefineSymbolsForGroup(BuildTargetGroup.Standalone);
            var splitDefines =
                new List<string>(defines.Split(new char[] { ';' }, System.StringSplitOptions.RemoveEmptyEntries));
            if (splitDefines.Contains("GAMEBASE_INITIALIZED"))
                splitDefines.Remove("GAMEBASE_INITIALIZED");
            defines = string.Join(";", splitDefines.ToArray());
            PlayerSettings.SetScriptingDefineSymbolsForGroup(BuildTargetGroup.Standalone, defines);
            Debug.Log($"[GAMEBASE_INITIALIZER] Gamebase local configs disabled ok");
        }

        private static void InvokeOnAllChildInitializers(Action<IChildProjectInitializer> action)
        {
            var initializerTypes = AppDomain.CurrentDomain.GetAssemblies().SelectMany(a => a.GetTypes());
                var i1 = initializerTypes.Where(t => typeof(IChildProjectInitializer).IsAssignableFrom(t) && t.IsClass && !t.IsAbstract).ToList();

            var initializers = new List<IChildProjectInitializer>();
            for (int i = 0; i < i1.Count; i++)
            {
                IChildProjectInitializer res = (IChildProjectInitializer)Activator.CreateInstance(i1[i], null);
                action.Invoke(res);
            }
        }

    }
}
