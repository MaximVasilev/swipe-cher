﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEditor;
using UnityEngine;

namespace Gamebase.Editor
{
    public static class CreateBaseArchitecture
    {
#if GAMEBASE_INITIALIZED
        [MenuItem("Gamebase/Create Base Architecture")]
#endif
        static void Create()
        {
            //Создает в папке скриптов основу для архитектуры (папки разных модулей с Assembly Definition в каждой)

            var assetsDir = Application.dataPath;
            var scriptsPath = $@"{assetsDir}\Scripts";
            if (!Directory.Exists(scriptsPath))
                Directory.CreateDirectory(scriptsPath);


            DirectoryInfo info = new DirectoryInfo(Application.dataPath);
            var gamebaseEditorAssembly = info.GetFiles("Gamebase.Editor.asmdef", SearchOption.AllDirectories);

            if (gamebaseEditorAssembly.Length > 0)
            {
                var template = File.ReadAllText(gamebaseEditorAssembly[0].FullName.Replace("Gamebase.Editor.asmdef", "AssemblyTemplate.json"));
                CreateModuleCatalogue(scriptsPath, "Gameplay", template);
                CreateModuleCatalogue(scriptsPath, "Graphics", template);
                CreateModuleCatalogue(scriptsPath, "Interface", template);
                CreateModuleCatalogue(scriptsPath, "Services", template);
                CreateModuleCatalogue(scriptsPath, "Utilities", template);
            }

            AssetDatabase.Refresh();
        }


        private static void CreateModuleCatalogue(string scriptsPath, string name, string template)
        {
            //Создаем папку
            var dir = $@"{scriptsPath}\{name}";
            if (!Directory.Exists(dir))
                Directory.CreateDirectory(dir);

            //Создаем файл Assembly Definition
            var replacedTemplate = template.Replace("{NAME}", name);
            var assemblyFile = $@"{dir}\{name}.asmdef";
            if (!File.Exists(assemblyFile))
                File.WriteAllText(assemblyFile, replacedTemplate);
        }
    }
}