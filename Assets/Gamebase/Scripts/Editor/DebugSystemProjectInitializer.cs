﻿using Gamebase.Miscellaneous;
using Gamebase.Systems.Debugging;
using UnityEngine;

namespace Gamebase.Editor
{
    public class DebugSystemProjectInitializer : IChildProjectInitializer
    {
        public void OnChildProjectInit()
        {
            CodeGenerator.CreateSettingsInstance<DebugSettings>("Debug Settings");
        }

        public void OnChildProjectReset()
        {
            CodeGenerator.RemoveSettingsInstance("Debug Settings");
        }
    }
}
