﻿using System.Collections;
using System.Collections.Generic;
using Gamebase.Editor;
using Gamebase.Miscellaneous;
using UnityEngine;

public class LevelsSystemProjectInitializer : IChildProjectInitializer
{
    public void OnChildProjectInit()
    {
        CodeGenerator.GenerateFromTemplate("LevelsSystem");
        CodeGenerator.GenerateFromTemplate("LevelConfiguration");
        CodeGenerator.GenerateFromTemplate("LevelProgression");
    }

    public void OnChildProjectReset()
    {
        CodeGenerator.RemoveGeneratedClass("LevelsSystem");
        CodeGenerator.RemoveGeneratedClass("LevelConfiguration");
        CodeGenerator.RemoveGeneratedClass("LevelProgression");
    }
}
