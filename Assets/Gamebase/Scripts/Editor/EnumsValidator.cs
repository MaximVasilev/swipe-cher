﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using UnityEditor;
using UnityEditor.Compilation;
using UnityEngine;

#if UNITY_EDITOR
namespace Gamebase.Editor
{
    /// <summary>
    /// Класс валидатора значений полей перечислений в проекте
    /// </summary>
    public static class EnumsValidator
    {
#if GAMEBASE_INITIALIZED
        [MenuItem("Gamebase/Validate Gamebase enums")]
#endif
        static public void DoValidateAllEnums()
        {
            //Звуки/музыка
            DoValidate(typeof(Gamebase.Systems.Sound.Sound));
            DoValidate(typeof(Gamebase.Systems.Sound.Music));
        }

        /// <summary>
        /// Валидация нужного перечисления
        /// </summary>
        /// <param name="enumType"></param>
        static public void DoValidate(Type enumType)
        {
            Type searchForType = typeof(Gamebase.Systems.Sound.Sound);
            //Перебираем все классы и ищем в них поля нужного перечисления
            var assemblies = CompilationPipeline.GetAssemblies(AssembliesType.Player).Select(e=>e.name).ToList();
            var projectAssemblies = AppDomain.CurrentDomain.GetAssemblies().Where(a => assemblies.Contains(a.GetName().Name) );
            var gameBaseAssemblyClasses = projectAssemblies.SelectMany(e=>e.DefinedTypes).Where(e => e.IsClass && !e.IsAbstract).ToList();
            var names = new HashSet<string>();
            foreach (var c in gameBaseAssemblyClasses)
                SearchForFieldsOfType(c, searchForType,ref names);

            //Ищем такие имена полей в ассетах
            var errorCounter = 0;
            DirectoryInfo dirInfo = new DirectoryInfo(Application.dataPath);
            var allAssetsFileNames = dirInfo.GetFiles("*.asset", SearchOption.AllDirectories);
            foreach(var assetFileName in allAssetsFileNames)
            {
                var assetData = File.ReadAllLines(assetFileName.FullName);
                foreach (var searchForFieldNames in names)
                {
                    foreach(var assetString in assetData)
                    {
                        var index = assetString.IndexOf(searchForFieldNames+":", 0);
                        if (index == -1)
                            continue;
                        //Проверка поля на существующее значение перечисления
                        int enumNumber = 0;
                        try
                        {
                            enumNumber = Int32.Parse(Regex.Match(assetString, @"-?\d+").Value);
                        }
                        catch (Exception)
                        {
                            Debug.LogWarning($"[ENUM_VALIDATOR] asset |{assetFileName.FullName}| contains wrong value type for |{searchForFieldNames}| - |{assetString}|");
                            errorCounter++;
                            continue;
                        }
                        var enumHaveThisNumber = Enum.IsDefined(searchForType, enumNumber);
                        if (!enumHaveThisNumber)
                        {
                            Debug.LogWarning($"[ENUM_VALIDATOR] asset |{assetFileName.FullName}| contains wrong enum |{searchForFieldNames}| value of |{enumNumber}|");
                            errorCounter++;
                        }
                    }
                }
            }
            Debug.Log($"[ENUM_VALIDATOR] Enum |{enumType.FullName}| validation finished. Found |{errorCounter}| errors");
        }

        /// <summary>
        /// Ищем поля нужного типа в классах проекта и составляем список их имен
        /// </summary>
        /// <param name="t">Класс, в котором ищем в данный момент</param>
        /// <param name="searchForType">Тип который ищем</param>
        /// <param name="names">Выходной список уникальных названий полей с таким типом</param>
        private static void SearchForFieldsOfType(Type t, Type searchForType,ref HashSet<string> names)
        {
            foreach(var m in t.GetMembers())
            {
                if (m.MemberType != MemberTypes.Field)
                    continue;
                var ttype = ((System.Reflection.FieldInfo)m).FieldType;
                if (ttype == searchForType)
                {
                    names.Add(m.Name);
                    continue;
                }
                if (!ttype.IsClass || ttype.IsAbstract || ttype.IsPrimitive || ttype == t)
                    continue;
                SearchForFieldsOfType(ttype, searchForType,ref names);
            }
        }

    }
}
#endif