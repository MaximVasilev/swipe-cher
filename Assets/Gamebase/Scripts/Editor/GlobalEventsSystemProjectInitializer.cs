using Gamebase.Miscellaneous;

namespace Gamebase.Editor
{
    public class GlobalEventsSystemProjectInitializer : IChildProjectInitializer
    {
        public void OnChildProjectInit()
        {
            DynamicEnums.Init("GlobalEventTypeEnum");
            CodeGenerator.CreateSettingsInstance<GlobalEventsSettings>("Global Events Settings");
        }

        public void OnChildProjectReset()
        {
            DynamicEnums.Reset("GlobalEventTypeEnum");
            CodeGenerator.RemoveSettingsInstance("Global Events Settings");
        }
    }
}
