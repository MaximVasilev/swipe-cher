﻿using System.Collections;
using System.Collections.Generic;
using Gamebase.Systems.GlobalEvents;
using UnityEngine;

public class Test : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        SettingTest s = SettingTest.LoadOrCreate();
        s.SaveToJSON();

        GlobalEventsSystem.Instance.Invoke(GlobalEventType.LevelInitialized);
    }

}
