﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Gamebase.Systems.Resources;
using UnityEngine;

public class Currency : Resource
{
    private const string playerPrefsKey = "Resource_Currency";

    private int _currentValue;
    private int CurrentValue
    {
        get { return _currentValue; }
        set
        {
            if (value < 0) return;

            _currentValue = value;
            PlayerPrefs.SetInt(playerPrefsKey, _currentValue);
        }
    }

    public override void GetSavedValue()
    {
        CurrentValue = PlayerPrefs.GetInt(playerPrefsKey, 0);
    }

    public override int GetCurrentValue()
    {
        return CurrentValue;
    }

    public override void Add(int count)
    {
        if (count < 0) return;

        CurrentValue += count;
    }

    public override void Set(int count)
    {
        CurrentValue = count;
    }
}
