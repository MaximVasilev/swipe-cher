﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Gamebase.Systems.Resources;
using UnityEngine;

public class LevelScorePoints : Resource
{
    private int _currentValue;
    private int CurrentValue
    {
        get { return _currentValue; }
        set
        {
            if (value < 0) return;

            _currentValue = value;
        }
    }

    public override void GetSavedValue()
    {
        CurrentValue = 0;
    }

    public override int GetCurrentValue()
    {
        return CurrentValue;
    }

    public override void Add(int count)
    {
        if (count < 0) return;

        CurrentValue += count;
    }

    public override void Set(int count)
    {
        CurrentValue = count;
    }
}

